﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoadGame
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoadGame))
        Me.lsvGames = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblDetails = New System.Windows.Forms.Label
        Me.btnLoad = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnRemove = New System.Windows.Forms.Button
        Me.lblNoGames = New System.Windows.Forms.Label
        Me.hlpYahtzee = New System.Windows.Forms.HelpProvider
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lsvGames
        '
        Me.lsvGames.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvGames.FullRowSelect = True
        Me.lsvGames.Location = New System.Drawing.Point(13, 32)
        Me.lsvGames.MultiSelect = False
        Me.lsvGames.Name = "lsvGames"
        Me.lsvGames.Size = New System.Drawing.Size(227, 154)
        Me.lsvGames.TabIndex = 0
        Me.lsvGames.UseCompatibleStateImageBehavior = False
        Me.lsvGames.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "# Players"
        Me.ColumnHeader1.Width = 57
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Game Start Time"
        Me.ColumnHeader2.Width = 144
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblDetails)
        Me.GroupBox1.Location = New System.Drawing.Point(246, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(202, 154)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Game Details"
        '
        'lblDetails
        '
        Me.lblDetails.Location = New System.Drawing.Point(6, 20)
        Me.lblDetails.Name = "lblDetails"
        Me.lblDetails.Size = New System.Drawing.Size(190, 131)
        Me.lblDetails.TabIndex = 0
        Me.lblDetails.Text = "Please select a Saved Game from list to view details"
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(246, 192)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(90, 23)
        Me.btnLoad.TabIndex = 2
        Me.btnLoad.Text = "Load Game"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Load Game"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(373, 192)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Location = New System.Drawing.Point(12, 192)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(90, 23)
        Me.btnRemove.TabIndex = 5
        Me.btnRemove.Text = "Delete Game"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'lblNoGames
        '
        Me.lblNoGames.AutoSize = True
        Me.lblNoGames.Location = New System.Drawing.Point(35, 64)
        Me.lblNoGames.Name = "lblNoGames"
        Me.lblNoGames.Size = New System.Drawing.Size(179, 13)
        Me.lblNoGames.TabIndex = 6
        Me.lblNoGames.Text = "There are no saved games available"
        Me.lblNoGames.Visible = False
        '
        'frmLoadGame
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(460, 227)
        Me.Controls.Add(Me.lblNoGames)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lsvGames)
        Me.HelpButton = True
        Me.hlpYahtzee.SetHelpKeyword(Me, "140")
        Me.hlpYahtzee.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.TopicId)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(476, 265)
        Me.MinimumSize = New System.Drawing.Size(476, 265)
        Me.Name = "frmLoadGame"
        Me.hlpYahtzee.SetShowHelp(Me, True)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Yahtzee - Load Game"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lsvGames As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblDetails As System.Windows.Forms.Label
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents lblNoGames As System.Windows.Forms.Label
    Friend WithEvents hlpYahtzee As System.Windows.Forms.HelpProvider
End Class
