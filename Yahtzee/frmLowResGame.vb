﻿Public Class frmLowResGame

    Dim intRollCount As Integer             'Current roll count - for dice animation
    Dim txtScoreBoxes(1, 17) As TextBox     'Player scoreboxes
    Dim lblPlayerNames(1) As Label          'Player name labels
    Dim tabPlayers(1) As TabPage            'Player tabs
    Dim pctDice(4) As PictureBox            'Dice pictureboxes

    Private Sub frmLowResGame_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If closeGameForm = False Then
            If MessageBox.Show("Would you like to save the current game before exiting?" & vbNewLine & vbNewLine & _
                               "Any unsaved progress will be lost.", My.Application.Info.Title, MessageBoxButtons.YesNo, _
                               MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                'save current game
                saveGame()
            End If
        End If
    End Sub

    Private Sub frmLowResGame_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set sizes of textbox and label arrays
        ReDim txtScoreBoxes(p.Length - 1, 17)
        ReDim lblPlayerNames(p.Length - 1)
        ReDim tabPlayers(p.Length - 1)

        'Create textboxes and labels
        createGameScreen()

        'Set up Roll button and counter
        setRollStatus()

        'Set help file location
        hlpYahtzee.HelpNamespace = My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm"
    End Sub

    Private Sub createGameScreen()
        'Set starting position for top player name/score labels
        Dim leftPos As Integer
        Select Case p.Length
            Case 1
                leftPos = 223
            Case 2
                leftPos = 204
            Case 3
                leftPos = 185
            Case 4
                leftPos = 166
        End Select

        'Generate dice
        For i As Integer = 0 To 4
            pctDice(i) = New PictureBox
            With pctDice(i)
                .Size = New Drawing.Size(60, 60)
                .Location = New Point(37, 127 + i * 66)
                .Margin = New Padding(3)
                .ImageLocation = My.Computer.FileSystem.CurrentDirectory & "\GameData\images\Dice_60x60\roll_dice.png"
                .Load()
                .Tag = i
                .BackColor = Color.Transparent
            End With
            Me.Controls.Add(pctDice(i))
            AddHandler pctDice(i).Click, AddressOf Dice_Click
        Next

        'Generate player name labels, scoring textboxes, tabs
        For i As Integer = 0 To p.Length - 1
            'Generate player name label
            lblPlayerNames(i) = New Label
            With lblPlayerNames(i)
                .AutoSize = False
                .Size = New Drawing.Size(88, 82)
                .Location = New Point(140 + i * 94, 33)
                .Text = p(i).Name & vbNewLine & vbNewLine & p(i).GrandTotal
                .Margin = New Padding(0)
                .TextAlign = ContentAlignment.MiddleCenter
                .Font = New Font("Microsoft Sans Serif", 10, FontStyle.Bold, GraphicsUnit.Point)
                .BackColor = colNameBg(i)
            End With
            Me.Controls.Add(lblPlayerNames(i))

            'Generate player tab
            tabPlayers(i) = New TabPage
            With tabPlayers(i)
                .Text = p(i).Name
                .Size = New Drawing.Size(362, 347)
                .BackColor = colNameBg(i)
                'Set background image
                If My.Computer.FileSystem.FileExists(My.Computer.FileSystem.CurrentDirectory & "\GameData\images\tab_bg.png") Then
                    .BackgroundImageLayout = ImageLayout.None
                    .BackgroundImage = Image.FromFile(My.Computer.FileSystem.CurrentDirectory & "\GameData\images\tab_bg.png")
                End If
            End With
            Me.tcPlayers.Controls.Add(tabPlayers(i))

            'Generate scoring textboxes
            For j As Integer = 0 To txtScoreBoxes.GetLength(1) - 1
                txtScoreBoxes(i, j) = New TextBox

                'Set general properties
                With txtScoreBoxes(i, j)
                    .Enabled = True
                    .ReadOnly = True
                    .Font = New Font("Microsoft Sans Serif", 10, FontStyle.Regular, GraphicsUnit.Point)
                    .Margin = New Padding(3, 3, 3, 3)
                    .Size = New Drawing.Size(75, 23)
                    .BorderStyle = BorderStyle.Fixed3D
                    .Text = ""
                    .BackColor = Color.White
                    .TextAlign = HorizontalAlignment.Center
                    .Cursor = Cursors.Default
                End With

                'Set locations and add textbox to tab
                If j >= 0 And j <= 7 Then
                    'Upper section scorebox
                    txtScoreBoxes(i, j).Location = New Point(84, 42 + j * 29)
                    Me.tabPlayers(i).Controls.Add(txtScoreBoxes(i, j))
                ElseIf j >= 8 And j <= 17 Then
                    'Lower section scorebox
                    txtScoreBoxes(i, j).Location = New Point(272, 42 + (j - 8) * 29)
                    Me.tabPlayers(i).Controls.Add(txtScoreBoxes(i, j))
                End If

                'Add other settings to textboxes
                If j >= 0 And j <= 5 Then 'Upper section scorebox
                    'Set tag - type of scorebox
                    txtScoreBoxes(i, j).Tag = j + 1

                    'Add handler - upper section scorebox
                    AddHandler txtScoreBoxes(i, j).MouseEnter, AddressOf txtUpperScoreBox_MouseEnter
                    AddHandler txtScoreBoxes(i, j).MouseLeave, AddressOf txtUpperScoreBox_MouseLeave
                    AddHandler txtScoreBoxes(i, j).Click, AddressOf txtUpperScoreBox_Click
                ElseIf j >= 8 And j <= 14 Then 'Lower section scorebox
                    'Set tag - type of scorebox
                    Select Case j
                        Case 8
                            txtScoreBoxes(i, j).Tag = "threeofkind"
                        Case 9
                            txtScoreBoxes(i, j).Tag = "fourofkind"
                        Case 10
                            txtScoreBoxes(i, j).Tag = "fullhouse"
                        Case 11
                            txtScoreBoxes(i, j).Tag = "smallstraight"
                        Case 12
                            txtScoreBoxes(i, j).Tag = "largestraight"
                        Case 13
                            txtScoreBoxes(i, j).Tag = "yahtzee"
                        Case 14
                            txtScoreBoxes(i, j).Tag = "chance"
                    End Select

                    'Add handler - lower section scorebox
                    AddHandler txtScoreBoxes(i, j).MouseEnter, AddressOf txtLowerScoreBox_MouseEnter
                    AddHandler txtScoreBoxes(i, j).MouseLeave, AddressOf txtLowerScoreBox_MouseLeave
                    AddHandler txtScoreBoxes(i, j).Click, AddressOf txtLowerScoreBox_Click
                ElseIf j = 6 Or j = 7 Or j = 15 Or j = 16 Or j = 17 Then 'Total and bonus textboxes
                    'Set background colour to standard control colour
                    txtScoreBoxes(i, j).BackColor = Color.FromArgb(255, 240, 240, 240)
                    'Set initial text to 0
                    txtScoreBoxes(i, j).Text = "0"
                End If
            Next
        Next

        'Display background image
        If My.Settings.showBgImage = True Then
            'Set path
            Dim bgImage As String = My.Computer.FileSystem.CurrentDirectory
            bgImage = bgImage & "\GameData\images\tab_main.png"

            'Show image if it exists
            If My.Computer.FileSystem.FileExists(bgImage) Then
                Me.BackgroundImageLayout = ImageLayout.Center 'Improves form speed
                Me.BackgroundImage = Image.FromFile(bgImage)
            End If
        End If

        'Set up player scoreboxes
        changePlayers()
    End Sub

    Public Sub changePlayers(Optional ByVal prevPlayer As Integer = -1)
        'Update player name labels
        If prevPlayer > -1 Then
            lblPlayerNames(prevPlayer).BorderStyle = BorderStyle.None
            lblPlayerNames(prevPlayer).Text = p(prevPlayer).Name & vbNewLine & vbNewLine & p(prevPlayer).GrandTotal
        End If
        lblPlayerNames(intCP).BorderStyle = BorderStyle.FixedSingle

        'Change selected player tab
        tcPlayers.SelectedTab = tabPlayers(intCP)

        'If more than one player, disable and enable appropriate textboxes
        'Disable all textboxes
        For i As Integer = 0 To p.Length - 1
            For j As Integer = 0 To 14
                If Not (j = 6 Or j = 7) Then 'Not upper section bonus or total
                    'Disable textbox
                    txtScoreBoxes(i, j).Enabled = False

                    'Change colour, depending on whether it is current player or not
                    If i = intCP Then
                        txtScoreBoxes(i, j).BackColor = Color.White
                    Else
                        txtScoreBoxes(i, j).BackColor = Color.FromArgb(255, 240, 240, 240)
                    End If
                End If
            Next
        Next

        'Enable non-filled textboxes for current player
        If p(intCP).Aces.Filled = False Then
            txtScoreBoxes(intCP, 0).Enabled = True
        End If
        If p(intCP).Twos.Filled = False Then
            txtScoreBoxes(intCP, 1).Enabled = True
        End If
        If p(intCP).Threes.Filled = False Then
            txtScoreBoxes(intCP, 2).Enabled = True
        End If
        If p(intCP).Fours.Filled = False Then
            txtScoreBoxes(intCP, 3).Enabled = True
        End If
        If p(intCP).Fives.Filled = False Then
            txtScoreBoxes(intCP, 4).Enabled = True
        End If
        If p(intCP).Sixes.Filled = False Then
            txtScoreBoxes(intCP, 5).Enabled = True
        End If
        If p(intCP).ThreeKind.Filled = False Then
            txtScoreBoxes(intCP, 8).Enabled = True
        End If
        If p(intCP).FourKind.Filled = False Then
            txtScoreBoxes(intCP, 9).Enabled = True
        End If
        If p(intCP).FullHouse.Filled = False Then
            txtScoreBoxes(intCP, 10).Enabled = True
        End If
        If p(intCP).SmallStraight.Filled = False Then
            txtScoreBoxes(intCP, 11).Enabled = True
        End If
        If p(intCP).LargeStraight.Filled = False Then
            txtScoreBoxes(intCP, 12).Enabled = True
        End If
        If p(intCP).Yahtzee.Filled = False Then
            txtScoreBoxes(intCP, 13).Enabled = True
        End If
        If p(intCP).Chance.Filled = False Then
            txtScoreBoxes(intCP, 14).Enabled = True
        End If
    End Sub

    '#####################################
    '######## Start Upper Section ########
    '#####################################

    Private Sub txtUpperScoreBox_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not p(intCP).Rolls = 0 Then
            Dim tb As TextBox
            tb = CType(sender, TextBox)

            'Update current scorebox to possible score
            tb.Text = addSameDice(CInt(tb.Tag))

            'Update Yahtzee Bonus textbox
            txtScoreBoxes(intCP, 15).Text = calcYahtzeeBonus(False, True)
        End If
    End Sub

    Private Sub txtUpperScoreBox_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not p(intCP).Rolls = 0 Then
            Dim tb As TextBox
            tb = CType(sender, TextBox)

            'Update current scorebox to nothing
            tb.Text = Nothing

            'Update Yahtzee Bonus textbox
            txtScoreBoxes(intCP, 15).Text = p(intCP).numYahtzeeBonus * 100 'calcYahtzeeBonus(False)
        End If
    End Sub

    Private Sub txtUpperScoreBox_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not p(intCP).Rolls = 0 Then
            Dim tb As TextBox
            tb = CType(sender, TextBox)

            'Calculate score
            Dim tempScore As Integer
            tempScore = addSameDice(CInt(tb.Tag))

            'Update player array with new score and filled = true
            Select Case CInt(tb.Tag)
                Case 1
                    p(intCP).Aces.Score = tempScore
                    p(intCP).Aces.Filled = True
                Case 2
                    p(intCP).Twos.Score = tempScore
                    p(intCP).Twos.Filled = True
                Case 3
                    p(intCP).Threes.Score = tempScore
                    p(intCP).Threes.Filled = True
                Case 4
                    p(intCP).Fours.Score = tempScore
                    p(intCP).Fours.Filled = True
                Case 5
                    p(intCP).Fives.Score = tempScore
                    p(intCP).Fives.Filled = True
                Case 6
                    p(intCP).Sixes.Score = tempScore
                    p(intCP).Sixes.Filled = True
            End Select

            'Update current scorebox to tempscore
            tb.Text = tempScore

            'Disable current scorebox - can't score here again
            tb.Enabled = False

            'Update Yahtzee Bonus textbox
            txtScoreBoxes(intCP, 15).Text = calcYahtzeeBonus(True)
            If checkYahtzee() = True Then
                p(intCP).numYahtzees += 1
            End If

            'Update total scores and bonuses, start new turn
            updateTotals()
            newTurn()
        End If

        'Give focus to roll button
        btnRoll.Focus()
    End Sub

    '#####################################
    '######## Start Lower Section ########
    '#####################################

    Private Sub txtLowerScoreBox_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not p(intCP).Rolls = 0 Then
            Dim tb As TextBox
            tb = CType(sender, TextBox)

            'Update current scorebox to possible score
            Select Case CStr(tb.Tag)
                Case "threeofkind"
                    tb.Text = calc3Kind()
                Case "fourofkind"
                    tb.Text = calc4Kind()
                Case "fullhouse"
                    tb.Text = calcFullHouse()
                Case "smallstraight"
                    tb.Text = calcSmlStr()
                Case "largestraight"
                    tb.Text = calcLrgStr()
                Case "yahtzee"
                    tb.Text = calcYahtzee()
                Case "chance"
                    tb.Text = calcChance()
            End Select

            'Update Yahtzee Bonus textbox
            txtScoreBoxes(intCP, 15).Text = calcYahtzeeBonus(False, True)
        End If
    End Sub

    Private Sub txtLowerScoreBox_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not p(intCP).Rolls = 0 Then
            Dim tb As TextBox
            tb = CType(sender, TextBox)

            'Update current scorebox to nothing
            tb.Text = Nothing

            'Update Yahtzee Bonus textbox
            txtScoreBoxes(intCP, 15).Text = p(intCP).numYahtzeeBonus * 100
        End If
    End Sub

    Private Sub txtLowerScoreBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Not p(intCP).Rolls = 0 Then
            Dim tb As TextBox
            tb = CType(sender, TextBox)

            Dim tempScore As Integer

            'Update current scorebox to possible score
            Select Case CStr(tb.Tag)
                Case "threeofkind"
                    tempScore = calc3Kind()
                    p(intCP).ThreeKind.Score = tempScore
                    p(intCP).ThreeKind.Filled = True
                Case "fourofkind"
                    tempScore = calc4Kind()
                    p(intCP).FourKind.Score = tempScore
                    p(intCP).FourKind.Filled = True
                Case "fullhouse"
                    tempScore = calcFullHouse()
                    p(intCP).FullHouse.Score = tempScore
                    p(intCP).FullHouse.Filled = True
                Case "smallstraight"
                    tempScore = calcSmlStr()
                    p(intCP).SmallStraight.Score = tempScore
                    p(intCP).SmallStraight.Filled = True
                Case "largestraight"
                    tempScore = calcLrgStr()
                    p(intCP).LargeStraight.Score = tempScore
                    p(intCP).LargeStraight.Filled = True
                Case "yahtzee"
                    tempScore = calcYahtzee()
                    p(intCP).Yahtzee.Score = tempScore
                    p(intCP).Yahtzee.Filled = True
                Case "chance"
                    tempScore = calcChance()
                    p(intCP).Chance.Score = tempScore
                    p(intCP).Chance.Filled = True
            End Select

            'Update current scorebox to tempscore
            tb.Text = tempScore

            'Disable current scorebox - can't score here again
            tb.Enabled = False

            'If yahtzee, increase counter by 1
            If checkYahtzee() = True Then
                p(intCP).numYahtzees += 1
            End If

            'If not Yahtzee scorebox, update Yahtzee bonus
            If Not tb.Tag = "yahtzee" Then
                'Update Yahtzee Bonus textbox
                txtScoreBoxes(intCP, 15).Text = calcYahtzeeBonus(True)
            End If

            'Update total scores and bonuses, start new turn
            updateTotals()
            newTurn()
        End If

        'Give focus to roll button
        btnRoll.Focus()
    End Sub

    Private Sub updateTotals()
        'Update upper bonus
        If p(intCP).UpperTotal >= 63 Then
            p(intCP).UpperBonus = 35
        End If

        'Set total and bonus textboxes
        txtScoreBoxes(intCP, 6).Text = p(intCP).UpperBonus
        txtScoreBoxes(intCP, 7).Text = p(intCP).UpperTotal
        txtScoreBoxes(intCP, 16).Text = p(intCP).LowerTotal
        txtScoreBoxes(intCP, 17).Text = p(intCP).GrandTotal
    End Sub

    '#####################################
    '########   Start Menu Strip  ########
    '#####################################

    Private Sub NewGameToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewGameToolStripMenuItem.Click
        'Ask if user wants to start new game
        If MessageBox.Show("Are you sure you want to start a new game?" & vbNewLine & vbNewLine & "All progress will be lost.", My.Application.Info.Title, _
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            'Show menu screen for user to start a new game
            closeGameForm = True 'Don't show save message on form close
            Me.Close()
            closeGameForm = False
            frmMainMenu.Show()
        End If
    End Sub

    Private Sub SaveGameToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveGameToolStripMenuItem.Click
        'Ask if user wants to save current game
        If MessageBox.Show("Would you like to save the current game?", My.Application.Info.Title, _
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            'Save current game
            saveGame()

            'Show success message
            MessageBox.Show("Game saved sucessfully.", My.Application.Info.Title, MessageBoxButtons.OK)
        End If
    End Sub

    Private Sub LoadGameToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadGameToolStripMenuItem.Click
        'Ask if user wants to load game
        If MessageBox.Show("Are you sure you want to load a saved game?" & vbNewLine & vbNewLine & "All progress in current game will be lost.", My.Application.Info.Title, _
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            'Show menu screen for user to start a new game
            closeGameForm = True
            Me.Close()
            closeGameForm = False
            frmLoadGame.Show()
        End If
    End Sub

    Private Sub HighScoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HighScoresToolStripMenuItem.Click
        'Show high scores window
        frmHighScores.Show()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        'Close game screen
        Me.Close()
    End Sub

    Private Sub OptionsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OptionsToolStripMenuItem.Click
        'Show options
        frmOptions.Show()
    End Sub

    Private Sub YahtzeeHelpToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles YahtzeeHelpToolStripMenuItem.Click
        'Show help - Game Screen topic
        Help.ShowHelp(Me, My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm", HelpNavigator.TopicId, "130")
    End Sub

    Private Sub YahtzeeRulesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles YahtzeeRulesToolStripMenuItem.Click
        'Show help - Yahtzee rules
        Help.ShowHelp(Me, My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm", HelpNavigator.TopicId, "200")
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        'Show about box
        frmAboutBox.Show()
    End Sub

    '#####################################
    '########  Start Dice & Roll  ########
    '#####################################

    Private Sub btnRoll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRoll.Click
        If p(intCP).Rolls = 0 Then
            'Reset dice array and roll dice
            rollDice(True)
        Else
            'Roll dice (no reset)
            rollDice()
        End If
    End Sub

    Private Sub Dice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If p(intCP).Rolls = 0 Then
            'Reset dice array and roll dice
            rollDice(True)
        Else
            'Keep current dice
            Dim pbD As PictureBox
            pbD = CType(sender, PictureBox)

            'Get which dice was clicked
            Dim intD As Integer
            intD = CInt(pbD.Tag)

            'Set dice to kep or not keep (opposite of current value)
            d(intD).Keep = Not d(intD).Keep

            If d(intD).Keep = True Then
                'Show red image (keep)
                pbD.ImageLocation = My.Computer.FileSystem.CurrentDirectory & "\GameData\images\Dice_60x60\dice_red_" & d(intD).Value & ".png"
                pbD.Load()
            Else
                'Show black image (don't keep)
                pbD.ImageLocation = My.Computer.FileSystem.CurrentDirectory & "\GameData\images\Dice_60x60\dice_black_" & d(intD).Value & ".png"
                pbD.Load()
            End If
        End If
    End Sub

    Private Sub tmrRollAni_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrRollAni.Tick
        intRollCount += 1
        If intRollCount < My.Settings.numAniDiceRolls Then
            'Display random dice
            For i As Integer = 0 To 4
                If d(i).Keep = False Then
                    pctDice(i).ImageLocation = My.Computer.FileSystem.CurrentDirectory & "\GameData\images\Dice_60x60\dice_black_" & randomValue(1, 6) & ".png"
                    pctDice(i).Load()
                End If
            Next
        Else
            'Display dice from roll

            'Disable timer and reset count
            intRollCount = 0
            tmrRollAni.Enabled = False

            If p(intCP).Rolls = 0 Then
                'Display 'Start Turn' image
                For i As Integer = 0 To 4
                    pctDice(i).ImageLocation = My.Computer.FileSystem.CurrentDirectory & "\GameData\images\Dice_60x60\roll_dice.png"
                    pctDice(i).Load()
                Next
            Else
                'Show dice images
                For i As Integer = 0 To 4
                    If d(i).Keep = True Then
                        pctDice(i).ImageLocation = My.Computer.FileSystem.CurrentDirectory & "\GameData\images\Dice_60x60\dice_red_" & d(i).Value & ".png"
                        pctDice(i).Load()
                    Else
                        pctDice(i).ImageLocation = My.Computer.FileSystem.CurrentDirectory & "\GameData\images\Dice_60x60\dice_black_" & d(i).Value & ".png"
                        pctDice(i).Load()
                    End If
                Next

                'Check for Yahtzee
                If checkYahtzee() = True Then
                    'Player has a Yahtzee
                    Dim strMessage As String = vbNewLine & vbNewLine & "Please score in a lower section scorebox if it is possible."
                    If p(intCP).Yahtzee.Filled = False Then
                        'Yantzee nor scored yet, so tell player to score in Yahtzee scorebox
                        strMessage = vbNewLine & "Please score this roll in the 'Yahtzee' scorebox."
                    ElseIf (p(intCP).Yahtzee.Filled = True And Not p(intCP).Yahtzee.Score = 0) And checkUpperSlot(intDiceVal(0)) = False Then
                        'Yahtzee box is filled with 50 and upper section scorebox not filled
                        'Tell player to score in upper section scorebox
                        Select Case intDiceVal(0)
                            Case 1
                                strMessage = vbNewLine & vbNewLine & "Please score this roll in the 'Aces' scorebox"
                            Case 2
                                strMessage = vbNewLine & vbNewLine & "Please score this roll in the 'Twos' scorebox"
                            Case 3
                                strMessage = vbNewLine & vbNewLine & "Please score this roll in the 'Threes' scorebox"
                            Case 4
                                strMessage = vbNewLine & vbNewLine & "Please score this roll in the 'Fours' scorebox"
                            Case 5
                                strMessage = vbNewLine & vbNewLine & "Please score this roll in the 'Fives' scorebox"
                            Case 6
                                strMessage = vbNewLine & vbNewLine & "Please score this roll in the 'Sixes' scorebox"
                        End Select
                    End If

                    'Show message to user
                    MessageBox.Show("Congratulations! You rolled a Yahtzee!" & strMessage, My.Application.Info.Title, _
                                     MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        End If
    End Sub

    Public Sub setRollStatus()
        'Enable and disable roll button depending on number of rolls
        If p(intCP).Rolls = 3 Then
            btnRoll.Enabled = False
        Else
            btnRoll.Enabled = True
        End If

        'Change text of roll button depending on number of rolls
        If p(intCP).Rolls = 0 Then
            btnRoll.Text = "Start Turn"

            'Display 'Start Turn' image
            For i As Integer = 0 To 4
                pctDice(i).ImageLocation = My.Computer.FileSystem.CurrentDirectory & "\GameData\images\Dice_60x60\roll_dice.png"
                pctDice(i).Load()
            Next
        Else
            btnRoll.Text = "Roll"
        End If

        'Show number of rolls completed
        lblRolls.Text = "Rolls: " & p(intCP).Rolls & " of 3"
    End Sub

    Public Sub generateLoadedGame()
        'Set up scoreboxes for all players from player array
        For i As Integer = 0 To p.Length - 1
            intCP = i
            If p(i).Aces.Filled = True Then
                txtScoreBoxes(intCP, 0).Text = p(i).Aces.Score
            End If
            If p(i).Twos.Filled = True Then
                txtScoreBoxes(intCP, 1).Text = p(i).Twos.Score
            End If
            If p(i).Threes.Filled = True Then
                txtScoreBoxes(intCP, 2).Text = p(i).Threes.Score
            End If
            If p(i).Fours.Filled = True Then
                txtScoreBoxes(intCP, 3).Text = p(i).Fours.Score
            End If
            If p(i).Fives.Filled = True Then
                txtScoreBoxes(intCP, 4).Text = p(i).Fives.Score
            End If
            If p(i).Sixes.Filled = True Then
                txtScoreBoxes(intCP, 5).Text = p(i).Sixes.Score
            End If
            If p(i).ThreeKind.Filled = True Then
                txtScoreBoxes(intCP, 8).Text = p(i).ThreeKind.Score
            End If
            If p(i).FourKind.Filled = True Then
                txtScoreBoxes(intCP, 9).Text = p(i).FourKind.Score
            End If
            If p(i).FullHouse.Filled = True Then
                txtScoreBoxes(intCP, 10).Text = p(i).FullHouse.Score
            End If
            If p(i).SmallStraight.Filled = True Then
                txtScoreBoxes(intCP, 11).Text = p(i).SmallStraight.Score
            End If
            If p(i).LargeStraight.Filled = True Then
                txtScoreBoxes(intCP, 12).Text = p(i).LargeStraight.Score
            End If
            If p(i).Yahtzee.Filled = True Then
                txtScoreBoxes(intCP, 13).Text = p(i).Yahtzee.Score
            End If
            If p(i).Chance.Filled = True Then
                txtScoreBoxes(intCP, 14).Text = p(i).Chance.Score
            End If

            updateTotals()
            lblPlayerNames(i).Text = p(i).Name & vbNewLine & vbNewLine & p(i).GrandTotal
        Next

        'Set current player
        intCP = getCP()

        'Set dice value array from loaded dice values
        setDiceArray()

        'Display Dice
        intRollCount = My.Settings.numAniDiceRolls
        tmrRollAni.Enabled = True

        'Disable relevant scoring boxes
        changePlayers()

        'Set up Roll button and counter
        setRollStatus()
    End Sub

    Private Sub btnYahtzee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnYahtzee.Click
        'Used to roll a Yahtzee during testing
        rollDice(False, True)
    End Sub
End Class