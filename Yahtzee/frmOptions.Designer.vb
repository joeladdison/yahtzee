﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOptions))
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbConfirmWinClose = New System.Windows.Forms.CheckBox
        Me.nudDiceRoll = New System.Windows.Forms.NumericUpDown
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.nudHighScores = New System.Windows.Forms.NumericUpDown
        Me.Label5 = New System.Windows.Forms.Label
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnDefaults = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.rbHiRes = New System.Windows.Forms.RadioButton
        Me.rbLowRes = New System.Windows.Forms.RadioButton
        Me.cbShowBgImg = New System.Windows.Forms.CheckBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.hlpYahtzee = New System.Windows.Forms.HelpProvider
        CType(Me.nudDiceRoll, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudHighScores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Number of Dice Rolls:"
        Me.ToolTip1.SetToolTip(Me.Label1, "The number of random dice to show during animation" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "of the dice when rolling.")
        '
        'cbConfirmWinClose
        '
        Me.cbConfirmWinClose.AutoSize = True
        Me.cbConfirmWinClose.Location = New System.Drawing.Point(28, 256)
        Me.cbConfirmWinClose.Name = "cbConfirmWinClose"
        Me.cbConfirmWinClose.Size = New System.Drawing.Size(132, 17)
        Me.cbConfirmWinClose.TabIndex = 1
        Me.cbConfirmWinClose.Text = "Confirm Program Close"
        Me.ToolTip1.SetToolTip(Me.cbConfirmWinClose, "Whether or not to show a confirmation message" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " when exiting the Yahtzee program." & _
                "")
        Me.cbConfirmWinClose.UseVisualStyleBackColor = True
        '
        'nudDiceRoll
        '
        Me.nudDiceRoll.Location = New System.Drawing.Point(141, 150)
        Me.nudDiceRoll.Maximum = New Decimal(New Integer() {30, 0, 0, 0})
        Me.nudDiceRoll.Minimum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudDiceRoll.Name = "nudDiceRoll"
        Me.nudDiceRoll.Size = New System.Drawing.Size(60, 20)
        Me.nudDiceRoll.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.nudDiceRoll, "The number of random dice to show during animation" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "of the dice when rolling.")
        Me.nudDiceRoll.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 130)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Dice Roll Animation"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 237)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Window Options"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 185)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "High Scores"
        '
        'nudHighScores
        '
        Me.nudHighScores.Location = New System.Drawing.Point(141, 205)
        Me.nudHighScores.Maximum = New Decimal(New Integer() {30, 0, 0, 0})
        Me.nudHighScores.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudHighScores.Name = "nudHighScores"
        Me.nudHighScores.Size = New System.Drawing.Size(60, 20)
        Me.nudHighScores.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.nudHighScores, "The maximum number of High Scores that should be saved." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "This applies to High Sco" & _
                "res with Yahtzees, as well as without Yahtzees.")
        Me.nudHighScores.Value = New Decimal(New Integer() {20, 0, 0, 0})
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(25, 207)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(115, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Maximum High Scores:"
        Me.ToolTip1.SetToolTip(Me.Label5, "The maximum number of High Scores that should be saved." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "This applies to High Sco" & _
                "res with Yahtzees, as well as without Yahtzees.")
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(116, 284)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(197, 284)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnDefaults
        '
        Me.btnDefaults.Location = New System.Drawing.Point(12, 284)
        Me.btnDefaults.Name = "btnDefaults"
        Me.btnDefaults.Size = New System.Drawing.Size(75, 23)
        Me.btnDefaults.TabIndex = 11
        Me.btnDefaults.Text = "Defaults"
        Me.btnDefaults.UseVisualStyleBackColor = True
        '
        'rbHiRes
        '
        Me.rbHiRes.AutoSize = True
        Me.rbHiRes.Location = New System.Drawing.Point(6, 19)
        Me.rbHiRes.Name = "rbHiRes"
        Me.rbHiRes.Size = New System.Drawing.Size(149, 17)
        Me.rbHiRes.TabIndex = 13
        Me.rbHiRes.TabStop = True
        Me.rbHiRes.Text = "Columns (High Resolution)"
        Me.ToolTip1.SetToolTip(Me.rbHiRes, "Layout with player scoreboxes in columns." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Especially suited for computers with a" & _
                " high screen resolution.")
        Me.rbHiRes.UseVisualStyleBackColor = True
        '
        'rbLowRes
        '
        Me.rbLowRes.AutoSize = True
        Me.rbLowRes.Location = New System.Drawing.Point(6, 42)
        Me.rbLowRes.Name = "rbLowRes"
        Me.rbLowRes.Size = New System.Drawing.Size(144, 17)
        Me.rbLowRes.TabIndex = 14
        Me.rbLowRes.TabStop = True
        Me.rbLowRes.Text = "Tabbed (Low Resolution)"
        Me.ToolTip1.SetToolTip(Me.rbLowRes, "Layout with a tab for each players' scoreboxes." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Especially suited for computers " & _
                "with a low screen resolution.")
        Me.rbLowRes.UseVisualStyleBackColor = True
        '
        'cbShowBgImg
        '
        Me.cbShowBgImg.AutoSize = True
        Me.cbShowBgImg.Location = New System.Drawing.Point(34, 101)
        Me.cbShowBgImg.Name = "cbShowBgImg"
        Me.cbShowBgImg.Size = New System.Drawing.Size(225, 17)
        Me.cbShowBgImg.TabIndex = 15
        Me.cbShowBgImg.Text = "Game Window - Show Background Image"
        Me.ToolTip1.SetToolTip(Me.cbShowBgImg, "Whether or not to display the background image on the Game Window." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Performance c" & _
                "an be improved by not showing the background image.")
        Me.cbShowBgImg.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Game Screen"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbLowRes)
        Me.GroupBox1.Controls.Add(Me.rbHiRes)
        Me.GroupBox1.Location = New System.Drawing.Point(28, 25)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(173, 70)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Layout"
        '
        'frmOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 319)
        Me.Controls.Add(Me.cbShowBgImg)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnDefaults)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.nudHighScores)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.nudDiceRoll)
        Me.Controls.Add(Me.cbConfirmWinClose)
        Me.Controls.Add(Me.Label1)
        Me.HelpButton = True
        Me.hlpYahtzee.SetHelpKeyword(Me, "160")
        Me.hlpYahtzee.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.TopicId)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(300, 357)
        Me.MinimumSize = New System.Drawing.Size(300, 357)
        Me.Name = "frmOptions"
        Me.hlpYahtzee.SetShowHelp(Me, True)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Yahtzee - Options"
        CType(Me.nudDiceRoll, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudHighScores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbConfirmWinClose As System.Windows.Forms.CheckBox
    Friend WithEvents nudDiceRoll As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents nudHighScores As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDefaults As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents rbHiRes As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbLowRes As System.Windows.Forms.RadioButton
    Friend WithEvents cbShowBgImg As System.Windows.Forms.CheckBox
    Friend WithEvents hlpYahtzee As System.Windows.Forms.HelpProvider
End Class
