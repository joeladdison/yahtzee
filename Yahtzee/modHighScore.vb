﻿Imports System.IO
Imports System.Security.Cryptography

Module modHighScore
    'Keys for encryption
    Private _key() As Byte = {211, 181, 227, 184, 82, 91, 42, 150, 153, 158, 154, 77, 47, 251, 72, 116, 36, 232, 72, 86, 43, 203, 9, 71, 131, 79, 173, 0, 84, 189, 47, 183}
    Private _iv() As Byte = {76, 160, 190, 29, 215, 253, 140, 110, 135, 105, 97, 107, 236, 224, 189, 235}

    Public Sub loadHS(Optional ByVal withoutYahtzee As Boolean = False)
        'Loads high scores from file into arraylist
        Try
            Dim strLine As String = Nothing
            Dim fileName As String = Nothing
            Dim strTemp() As String 'Temporary array for string split

            'Clear hs arraylist
            hs.Clear()

            If withoutYahtzee = True Then
                'High scores without Yahtzees
                fileName = My.Computer.FileSystem.CurrentDirectory & "\GameData\HighScoresNY.bin"
            Else
                'High scores with Yahtzees
                fileName = My.Computer.FileSystem.CurrentDirectory & "\GameData\HighScoresWY.bin"
            End If

            If My.Computer.FileSystem.FileExists(fileName) = False Then
                'If file doesn't exist, do not load scores from file
                Exit Sub
            End If

            'Create or open the specified file. 
            Dim fStream As FileStream = File.Open(fileName, FileMode.Open)

            'Create a new Rijndael object.
            Dim RijndaelAlg As Rijndael = Rijndael.Create

            'Create a CryptoStream using the FileStream and the default key and initialization vector (IV).
            Dim cStream As New CryptoStream(fStream, RijndaelAlg.CreateDecryptor(_key, _iv), CryptoStreamMode.Read)

            'Create a StreamReader using the CryptoStream.
            Dim sReader As New StreamReader(cStream)

            Try
                Do
                    'Get high scores from file
                    strLine = sReader.ReadLine()
                    If Not strLine = Nothing Then
                        Dim tempHS As New HighScore

                        'Get high score data from current line
                        strTemp = strLine.Split(";")
                        tempHS.Name = strTemp(0)
                        tempHS.Score = strTemp(1)
                        tempHS.numYahtzees = strTemp(2)
                        tempHS.gameDate = strTemp(3)

                        'Add high score to arraylist
                        hs.Add(tempHS)
                    End If
                Loop Until strLine Is Nothing
            Catch e As Exception
                Console.WriteLine("An error occurred: {0}", e.Message)
            Finally
                'Close the streams and close the file
                sReader.Close()
                cStream.Close()
                fStream.Close()
            End Try

            'Sort and reverse the high scores ArrayList (desc order)
            hs.Sort()
            hs.Reverse()

            'Delete extra high scores, if there are any
            trimHS(withoutYahtzee)
        Catch e As CryptographicException
            Console.WriteLine("A Cryptographic error occurred: {0}", e.Message)
        Catch e As UnauthorizedAccessException
            Console.WriteLine("A file error occurred: {0}", e.Message)
        End Try
    End Sub

    Public Sub saveHS(Optional ByVal withoutYahtzee As Boolean = False)
        'Saves high scores arraylist to file
        Try
            Dim fileName As String

            'Set filepath for high scores file
            If withoutYahtzee = True Then
                fileName = My.Computer.FileSystem.CurrentDirectory & "\GameData\HighScoresNY.bin"
            Else
                fileName = My.Computer.FileSystem.CurrentDirectory & "\GameData\HighScoresWY.bin"
            End If

            'Create directory if it doesn't already exist
            If Not My.Computer.FileSystem.DirectoryExists(My.Computer.FileSystem.CurrentDirectory & "\GameData") Then
                My.Computer.FileSystem.CreateDirectory(My.Computer.FileSystem.CurrentDirectory & "\GameData")
            End If

            ' Create or open the specified file.
            Dim fStream As FileStream = File.Open(fileName, FileMode.OpenOrCreate)

            ' Create a new Rijndael object.
            Dim RijndaelAlg As Rijndael = Rijndael.Create

            ' Create a CryptoStream using the FileStream and the default key and initialization vector (IV).
            Dim cStream As New CryptoStream(fStream, RijndaelAlg.CreateEncryptor(_key, _iv), _
                                           CryptoStreamMode.Write)

            ' Create a StreamWriter using the CryptoStream.
            Dim sWriter As New StreamWriter(cStream)

            'Write high scores to file
            For i As Integer = 0 To hs.Count - 1
                Dim tempHS As New HighScore
                tempHS = hs(i)

                ' Write the data to the stream to encrypt it.
                sWriter.WriteLine(tempHS.Name & ";" & tempHS.Score & ";" & tempHS.numYahtzees & ";" & tempHS.gameDate & ";")
            Next

            'Close the streams and close the file
            sWriter.Close()
            cStream.Close()
            fStream.Close()
        Catch e As CryptographicException
            Console.WriteLine("A Cryptographic error occurred: {0}", e.Message)
        Catch e As UnauthorizedAccessException
            Console.WriteLine("A file error occurred: {0}", e.Message)
        End Try
    End Sub

    Public Function checkHS(ByVal player As Integer) As Boolean
        'Checks if player's score is a new high score, and if it is, adds it to high scores list
        checkHS = False

        'load high scores (with, or without yahtzees)
        If p(player).numYahtzees = 0 Then
            'Load high scores without Yathzees
            loadHS(True)
        Else
            'Load high scores with Yahtzees
            loadHS(False)
        End If

        If hs.Count = My.Settings.maxHighScores Then
            'High scores list is full
            'Need to remove lowest high score IF the current score is higher than it
            Dim tempHS As New HighScore
            tempHS = hs(hs.Count - 1)
            If p(player).GrandTotal > tempHS.Score Then
                'Current score is highest than lowest high score
                'Remove lowest high score
                hs.RemoveAt(hs.Count - 1)

                'Add new high score
                tempHS.Name = p(player).Name
                tempHS.Score = p(player).GrandTotal
                tempHS.numYahtzees = p(player).numYahtzees
                tempHS.gameDate = Now
                hs.Add(tempHS)
                checkHS = True
            End If
        Else
            'High scores list is not full, so add current score to the list
            Dim tempHS As New HighScore
            tempHS.Name = p(player).Name
            tempHS.Score = p(player).GrandTotal
            tempHS.numYahtzees = p(player).numYahtzees
            tempHS.gameDate = Now
            hs.Add(tempHS)
            checkHS = True
        End If

        'Sort and reverse the high scores ArrayList (desc order)
        hs.Sort()
        hs.Reverse()

        'Save scores to file
        If p(player).numYahtzees = 0 Then
            'Save high scores without Yahtzees
            saveHS(True)
        Else
            'Save high scores with Yahtzees
            saveHS()
        End If
        Return checkHS
    End Function

    Public Sub clearHS()
        'Clear all high scores, and delete files
        Dim fileName As String = Nothing

        'Clear hs arraylist
        hs.Clear()

        'Delete file for high scores without Yahtzees
        fileName = My.Computer.FileSystem.CurrentDirectory & "\GameData\HighScoresNY.bin"
        If My.Computer.FileSystem.FileExists(fileName) = True Then
            My.Computer.FileSystem.DeleteFile(fileName)
        End If

        'Delete file for high scores with Yahtzees
        fileName = My.Computer.FileSystem.CurrentDirectory & "\GameData\HighScoresWY.bin"
        If My.Computer.FileSystem.FileExists(fileName) = True Then
            My.Computer.FileSystem.DeleteFile(fileName)
        End If
    End Sub

    Public Sub trimHS(Optional ByVal withoutYahtzee As Boolean = False)
        'If there are more high scores saved than should be, this will delete extra high scores
        'More scores will occur if maxHighScores setting is decreased

        'Check for and remove additional high scores
        If hs.Count > My.Settings.maxHighScores Then
            Dim remCount As Integer
            remCount = hs.Count - My.Settings.maxHighScores
            hs.RemoveRange(My.Settings.maxHighScores, remCount)

            'Save new high scores list
            saveHS(withoutYahtzee)
        End If
    End Sub
End Module