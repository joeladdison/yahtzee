﻿Imports System.IO
Imports System.Security.Cryptography

Module modSaveLoadGame
    'Keys for encryption
    Private _key() As Byte = {211, 181, 227, 184, 82, 91, 42, 150, 153, 158, 154, 77, 47, 251, 72, 116, 36, 232, 72, 86, 43, 203, 9, 71, 131, 79, 173, 0, 84, 189, 47, 183}
    Private _iv() As Byte = {76, 160, 190, 29, 215, 253, 140, 110, 135, 105, 97, 107, 236, 224, 189, 235}

    Public gameList As ArrayList = New ArrayList 'Stores game details for load game screen

    Public Structure gameDetails
        'Structure for game details (used in details pane on load game screen)
        Dim numPlayers As Integer
        Dim playerNames As String
        Dim gameStateDate As Date
        Dim fileName As String
    End Structure

    Public Sub loadGame(ByVal fileName As String)
        'Loads the selected game from file
        Try
            Dim strTemp() As String 'Temporary array for string split

            If My.Computer.FileSystem.FileExists(fileName) = False Then
                'If file doesn't exist, do not load scores from file
                Exit Sub
            End If

            'Create or open the specified file. 
            Dim fStream As FileStream = File.Open(fileName, FileMode.Open)

            'Create a new Rijndael object.
            Dim RijndaelAlg As Rijndael = Rijndael.Create

            'Create a CryptoStream using the FileStream and the default key and initialization vector (IV).
            Dim cStream As New CryptoStream(fStream, RijndaelAlg.CreateDecryptor(_key, _iv), CryptoStreamMode.Read)

            'Create a StreamReader using the CryptoStream.
            Dim sReader As New StreamReader(cStream)

            Try
                'Read the file, and save data into variables
                'Number of players
                ReDim p(CInt(sReader.ReadLine()) - 1)

                'Names of players - not used here
                sReader.ReadLine()

                'Start date and time
                dteStartTime = CDate(sReader.ReadLine())

                'Dice values
                For i As Integer = 0 To 4
                    strTemp = sReader.ReadLine.Split(";")
                    d(i).Value = strTemp(0)
                    d(i).Keep = CBool(strTemp(1))
                Next

                'Player data
                For i As Integer = 0 To p.Length - 1
                    p(i).Name = sReader.ReadLine
                    p(i).Turn = CBool(sReader.ReadLine)
                    p(i).Rolls = CInt(sReader.ReadLine)
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).Aces.Score = CInt(strTemp(0))
                    p(i).Aces.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).Twos.Score = CInt(strTemp(0))
                    p(i).Twos.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).Threes.Score = CInt(strTemp(0))
                    p(i).Threes.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).Fours.Score = CInt(strTemp(0))
                    p(i).Fours.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).Fives.Score = CInt(strTemp(0))
                    p(i).Fives.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).Sixes.Score = CInt(strTemp(0))
                    p(i).Sixes.Filled = CBool(strTemp(1))
                    p(i).UpperBonus = CInt(sReader.ReadLine)

                    strTemp = sReader.ReadLine.Split(";")
                    p(i).ThreeKind.Score = CInt(strTemp(0))
                    p(i).ThreeKind.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).FourKind.Score = CInt(strTemp(0))
                    p(i).FourKind.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).FullHouse.Score = CInt(strTemp(0))
                    p(i).FullHouse.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).SmallStraight.Score = CInt(strTemp(0))
                    p(i).SmallStraight.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).LargeStraight.Score = CInt(strTemp(0))
                    p(i).LargeStraight.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).Yahtzee.Score = CInt(strTemp(0))
                    p(i).Yahtzee.Filled = CBool(strTemp(1))
                    strTemp = sReader.ReadLine.Split(";")
                    p(i).Chance.Score = CInt(strTemp(0))
                    p(i).Chance.Filled = CBool(strTemp(1))
                    p(i).numYahtzeeBonus = CInt(sReader.ReadLine)
                    p(i).numYahtzees = CInt(sReader.ReadLine)
                Next

            Catch e As Exception
                Console.WriteLine("An error occurred: {0}", e.Message)
            Finally
                'Close the streams and close the file
                sReader.Close()
                cStream.Close()
                fStream.Close()
            End Try

            'Set up game
            If My.Settings.gameSize = "hires" Then
                frmHiResGame.Show()
                frmHiResGame.generateLoadedGame()
            Else
                frmLowResGame.Show()
                frmLowResGame.generateLoadedGame()
            End If
        Catch e As CryptographicException
            Console.WriteLine("A Cryptographic error occurred: {0}", e.Message)
        Catch e As UnauthorizedAccessException
            Console.WriteLine("A file error occurred: {0}", e.Message)
        End Try
    End Sub

    Public Sub saveGame()
        'Saves the current game to file
        Try
            Dim fileName As String
            fileName = My.Computer.FileSystem.CurrentDirectory & "\SaveData\"
            fileName = fileName & p.Length.ToString & "Player_" & dteStartTime.ToString("yyyyMMdd") & "_" & dteStartTime.ToString("HHmmss") & ".sav"

            'Create directory if it doesn't already exist
            If Not My.Computer.FileSystem.DirectoryExists(My.Computer.FileSystem.CurrentDirectory & "\SaveData") Then
                My.Computer.FileSystem.CreateDirectory(My.Computer.FileSystem.CurrentDirectory & "\SaveData")
            End If

            ' Create or open the specified file.
            Dim fStream As FileStream = File.Open(fileName, FileMode.OpenOrCreate)

            ' Create a new Rijndael object.
            Dim RijndaelAlg As Rijndael = Rijndael.Create

            ' Create a CryptoStream using the FileStream and the default key and initialization vector (IV).
            Dim cStream As New CryptoStream(fStream, RijndaelAlg.CreateEncryptor(_key, _iv), _
                                           CryptoStreamMode.Write)

            ' Create a StreamWriter using the CryptoStream.
            Dim sWriter As New StreamWriter(cStream)
            Dim strLine As String = Nothing

            'Write names of players and start time
            sWriter.WriteLine(p.Length) 'Number of players
            For i As Integer = 0 To p.Length - 1
                strLine = strLine & p(i).Name.ToString & ";"
            Next
            sWriter.WriteLine(strLine) 'Player names
            sWriter.WriteLine(dteStartTime) 'Game start time

            'Write values of dice array
            For i As Integer = 0 To 4
                sWriter.WriteLine(d(i).Value.ToString & ";" & d(i).Keep) 'Dice value and keep
            Next

            'Write player structure to file
            For i As Integer = 0 To p.Length - 1
                sWriter.WriteLine(p(i).Name)
                sWriter.WriteLine(p(i).Turn)
                sWriter.WriteLine(p(i).Rolls)
                sWriter.WriteLine(p(i).Aces.Score & ";" & p(i).Aces.Filled)
                sWriter.WriteLine(p(i).Twos.Score & ";" & p(i).Twos.Filled)
                sWriter.WriteLine(p(i).Threes.Score & ";" & p(i).Threes.Filled)
                sWriter.WriteLine(p(i).Fours.Score & ";" & p(i).Fours.Filled)
                sWriter.WriteLine(p(i).Fives.Score & ";" & p(i).Fives.Filled)
                sWriter.WriteLine(p(i).Sixes.Score & ";" & p(i).Sixes.Filled)
                sWriter.WriteLine(p(i).UpperBonus)
                sWriter.WriteLine(p(i).ThreeKind.Score & ";" & p(i).ThreeKind.Filled)
                sWriter.WriteLine(p(i).FourKind.Score & ";" & p(i).FourKind.Filled)
                sWriter.WriteLine(p(i).FullHouse.Score & ";" & p(i).FullHouse.Filled)
                sWriter.WriteLine(p(i).SmallStraight.Score & ";" & p(i).SmallStraight.Filled)
                sWriter.WriteLine(p(i).LargeStraight.Score & ";" & p(i).LargeStraight.Filled)
                sWriter.WriteLine(p(i).Yahtzee.Score & ";" & p(i).Yahtzee.Filled)
                sWriter.WriteLine(p(i).Chance.Score & ";" & p(i).Chance.Filled)
                sWriter.WriteLine(p(i).numYahtzeeBonus)
                sWriter.WriteLine(p(i).numYahtzees)
            Next

            'Close the streams and close the file
            sWriter.Close()
            cStream.Close()
            fStream.Close()
        Catch e As CryptographicException
            Console.WriteLine("A Cryptographic error occurred: {0}", e.Message)
        Catch e As UnauthorizedAccessException
            Console.WriteLine("A file error occurred: {0}", e.Message)
        End Try
    End Sub

    Public Sub loadDetails(ByVal fileName As String)
        'Loads details of all saved games into arraylist
        Try
            Dim tempDetails As gameDetails 'Stores temporary details

            'Create or open the specified file. 
            Dim fStream As FileStream = File.Open(fileName, FileMode.Open)

            'Create a new Rijndael object.
            Dim RijndaelAlg As Rijndael = Rijndael.Create

            'Create a CryptoStream using the FileStream and the default key and initialization vector (IV).
            Dim cStream As New CryptoStream(fStream, RijndaelAlg.CreateDecryptor(_key, _iv), CryptoStreamMode.Read)

            'Create a StreamReader using the CryptoStream.
            Dim sReader As New StreamReader(cStream)


            'Read the file, and save data into variables
            tempDetails.numPlayers = sReader.ReadLine 'Number of players
            tempDetails.playerNames = sReader.ReadLine 'Names of players
            tempDetails.gameStateDate = sReader.ReadLine 'Start date and time
            tempDetails.fileName = fileName
            'Add details to arraylist
            gameList.Add(tempDetails)

            'Close the streams and close the file
            sReader.Close()
            cStream.Close()
            fStream.Close()
        Catch e As CryptographicException
            Console.WriteLine("A Cryptographic error occurred: {0}", e.Message)
        Catch e As UnauthorizedAccessException
            Console.WriteLine("A file error occurred: {0}", e.Message)
        End Try
    End Sub

    Public Sub removeSavedGame(ByVal fileName As String)
        'Delete file
        If My.Computer.FileSystem.FileExists(fileName) = True Then
            My.Computer.FileSystem.DeleteFile(fileName)
        End If
    End Sub
End Module