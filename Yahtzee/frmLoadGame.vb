﻿Public Class frmLoadGame

    Private Sub frmLoadGame_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Clear saved game details
        gameList.Clear()
    End Sub

    Private Sub frmLoadGame_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set help file location
        hlpYahtzee.HelpNamespace = My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm"

        'Get all saved games
        Dim saveGamePath As String
        saveGamePath = My.Computer.FileSystem.CurrentDirectory & "\SaveData"

        'list games if directory exists
        If My.Computer.FileSystem.DirectoryExists(saveGamePath) Then
            For Each fileName As String In My.Computer.FileSystem.GetFiles(saveGamePath, FileIO.SearchOption.SearchTopLevelOnly, "*.sav")
                If Not My.Computer.FileSystem.FileExists(fileName) = False Then
                    'If file doesn't exist, do not load scores from file
                    loadDetails(fileName)
                End If
            Next

            'Add games to listview
            Dim tempDetails As gameDetails
            Dim str(1) As String
            Dim itm As ListViewItem
            For i As Integer = 0 To gameList.Count - 1
                tempDetails = gameList(i)
                str(0) = tempDetails.numPlayers.ToString
                str(1) = tempDetails.gameStateDate.ToString
                itm = New ListViewItem(str)
                lsvGames.Items.Add(itm)
            Next
        End If
        If gameList.Count = 0 Then
            'No saved games
            lsvGames.Enabled = False
            lblNoGames.Visible = True
            btnLoad.Enabled = False
            btnRemove.Enabled = False
        End If
    End Sub

    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        If lsvGames.SelectedIndices.Count = 1 Then
            'Load game
            Dim tempDetails As gameDetails
            tempDetails = gameList(lsvGames.SelectedIndices.Item(0))
            loadGame(tempDetails.fileName)
            Me.Close()
        Else
            'No game selected in list
            MessageBox.Show("Please select a saved game to load.", My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub lsvGames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvGames.SelectedIndexChanged
        If lsvGames.SelectedIndices.Count = 1 Then

            'Show details in side pane
            Dim strTemp() As String
            Dim tempDetails As gameDetails
            tempDetails = gameList(lsvGames.SelectedIndices.Item(0))
            strTemp = tempDetails.playerNames.Split(";")
            lblDetails.Text = "# of Players: " & tempDetails.numPlayers & vbNewLine & vbNewLine
            lblDetails.Text = lblDetails.Text & "Game Date: " & tempDetails.gameStateDate.ToString & vbNewLine & vbNewLine
            lblDetails.Text = lblDetails.Text & "Players: " & vbNewLine

            For i As Integer = 0 To strTemp.Length - 1
                lblDetails.Text = lblDetails.Text & strTemp(i) & vbNewLine
            Next
        Else
            'No game selected in list
            lblDetails.Text = "Please select a Saved Game from list to view details"
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If lsvGames.SelectedIndices.Count = 1 Then
            If MessageBox.Show("Are you sure you want to delete the selected saved game?" & vbNewLine & vbNewLine & "This action cannot be undone.", _
                               My.Application.Info.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                'Delete Game
                Dim tempDetails As gameDetails
                tempDetails = gameList(lsvGames.SelectedIndices.Item(0))
                removeSavedGame(tempDetails.fileName)

                'Remove game from listview and arraylist
                gameList.RemoveAt(lsvGames.SelectedIndices.Item(0))
                lsvGames.Items.RemoveAt(lsvGames.SelectedIndices.Item(0))

                If gameList.Count = 0 Then
                    'No saved games
                    lsvGames.Enabled = False
                    lblNoGames.Visible = True
                    btnLoad.Enabled = False
                    btnRemove.Enabled = False
                End If
            End If
        Else
            'No game selected
            MessageBox.Show("Please select a saved game to delete.", My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
End Class