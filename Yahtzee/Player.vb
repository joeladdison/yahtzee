﻿Public Structure Player
    'Structure for all Player data - name, rolls, scores
    'Public variables
    Public Name As String               'Stores name of player
    Public Turn As Boolean              'True if currently player’s turn, False otherwise
    Public Rolls As Integer             'Stores number of rolls
    Public Aces As Scorebox             'Stores value of aces (1) score, and whether scorebox is filled or not
    Public Twos As Scorebox             'Stores value of twos (2) score, and whether scorebox is filled or not
    Public Threes As Scorebox           'Stores value of threes (3) score, and whether scorebox is filled or not
    Public Fours As Scorebox            'Stores value of fours (4) score, and whether scorebox is filled or not
    Public Fives As Scorebox            'Stores value of fives (5) score, and whether scorebox is filled or not
    Public Sixes As Scorebox            'Stores value of sixes (6) score, and whether scorebox is filled or not
    Public UpperBonus As Integer        'Stores value of upper bonus
    Public ThreeKind As Scorebox        'Stores value of 3 of a kind score, and whether scorebox is filled or not
    Public FourKind As Scorebox         'Stores value of 4 of a kind score, and whether scorebox is filled or not
    Public FullHouse As Scorebox        'Stores value of full house score, and whether scorebox is filled or not
    Public SmallStraight As Scorebox    'Stores value of small straight score, and whether scorebox is filled or not
    Public LargeStraight As Scorebox    'Stores value of large straight score, and whether scorebox is filled or not
    Public Yahtzee As Scorebox          'Stores value of yahtzee score, and whether scorebox is filled or not
    Public Chance As Scorebox           'Stores value of chance score, and whether scorebox is filled or not
    Public numYahtzeeBonus As Integer   'Stores number of Yahtzee bonuses
    Public numYahtzees As Integer       'Stores number of Yahtzees player scored

    'Properties - total scores
    Public ReadOnly Property UpperTotal() As Integer 'Property that returns calculation of grand total
        Get
            Return Aces.Score + Twos.Score + Threes.Score + Fours.Score + Fives.Score + Sixes.Score + UpperBonus
        End Get
    End Property

    Public ReadOnly Property LowerTotal() As Integer 'Property that returns calculation of upper total
        Get
            Return ThreeKind.Score + FourKind.Score + FullHouse.Score + SmallStraight.Score + LargeStraight.Score + Yahtzee.Score + Chance.Score + (numYahtzeeBonus * 100)
        End Get
    End Property

    Public ReadOnly Property GrandTotal() As Integer 'Property that returns calculation of lower total
        Get
            Return UpperTotal + LowerTotal
        End Get
    End Property
End Structure

Public Structure Scorebox
    'Structure for scoreboxes - score value, if scorebox has been filled
    Public Score As Integer             'Stores value of score
    Public Filled As Boolean            'Stores whether or not scorebox has been filled by player
End Structure

Public Structure Dice
    'Structure for dice - value, if dice is to be kept on re-roll
    Public Value As Integer             'Stores value of dice
    Public Keep As Boolean              'True if dice being kept, False if dice not being kept
End Structure

Public Class HighScore
    'Class for High scores - stores data from text file during runtime
    Implements IComparable

    Public Name As String               'Stores name of player that achieved high score
    Public Score As Integer             'Stores value of high score
    Public numYahtzees As Integer       'Stores number of Yahtzees in the high score
    Public gameDate As Date             'Stores date high score was achieved on

    Public Function CompareTo(ByVal obj As Object) As Integer _
      Implements System.IComparable.CompareTo
        'Function for sorting HighScore
        If Not TypeOf obj Is HighScore Then
            Throw New Exception("Object is not Highscore")
        End If
        Dim Compare As HighScore = CType(obj, HighScore)
        Dim result As Integer = Me.Score.CompareTo(Compare.Score)

        If result = 0 Then
            result = Me.Score.CompareTo(Compare.Score)
        End If
        Return result
    End Function
End Class