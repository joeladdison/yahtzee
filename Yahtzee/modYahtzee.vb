﻿Module modYahtzee

    Public d(4) As Dice                 'Dice data
    Public p(3) As Player               'All player data
    Public intDiceVal(4) As Integer     'Values of dice
    Public intCP As Integer             'Number of current player
    Public closeGameForm As Boolean     'Whether to close the game form or show confirmation message
    Public hs As New ArrayList          'Stores high score
    Public dteStartTime As Date         'Stores the start time of the game

    'Background colours for player name labels
    Public colNameBg() As Color = {Color.Aqua, Color.LightSalmon, Color.Thistle, Color.PaleGreen}

    Public Sub newGame(ByVal players As Integer, ByVal names() As String)
        'Set array to be length of number of players
        ReDim p(players - 1)

        'Set names for each player
        For i As Integer = 0 To players - 1
            p(i).Name = names(i)
        Next

        'Set current player to be Player 1
        p(0).Turn = True
        intCP = getCP()

        'Set start time of game
        dteStartTime = Now

        'Load game form
        If My.Settings.gameSize = "hires" Then
            frmHiResGame.Show()
        Else
            frmLowResGame.Show()
        End If
    End Sub

    Public Sub newTurn()
        'If multi-player, change to next player
        Dim intPrevP As Integer
        If Not p.Length = 1 Then
            p(intCP).Turn = False
            If (intCP + 1) > (p.Length - 1) Then
                p(0).Turn = True
            Else
                p(intCP + 1).Turn = True
            End If
            intPrevP = intCP
            intCP = getCP()
        End If

        'Set current player's rolls to 0
        p(intCP).Rolls = 0

        If Not filledScoreboxes(intCP) = 13 Then
            'Game is still going - change to next player
            If My.Settings.gameSize = "hires" Then
                frmHiResGame.changePlayers(intPrevP)
                frmHiResGame.setRollStatus()
            Else
                frmLowResGame.changePlayers(intPrevP)
                frmLowResGame.setRollStatus()
            End If
        Else
            'Game is over - all scoreboxes filled for all players

            'Check for high scores
            For i As Integer = 0 To p.Length - 1
                If checkHS(i) = True Then
                    MessageBox.Show("Congratulations " & p(i).Name & "! You got a new High Score of " & p(i).GrandTotal & "!", _
                                     My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Next

            'Ask if player wants to play again
            If MessageBox.Show("Your game is finished." & vbNewLine & "Would you like to play Yahtzee again?", _
                               My.Application.Info.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
                'Game is finished, but player wants to play again

                'Get names of current players for new game
                Dim strNames(p.Length - 1) As String
                For i As Integer = 0 To p.Length - 1
                    strNames(i) = p(i).Name
                Next

                'Close current game form
                closeGameForm = True
                If My.Settings.gameSize = "hires" Then
                    frmHiResGame.Close()
                Else
                    frmLowResGame.Close()
                End If
                closeGameForm = False

                'Start new game
                newGame(p.Length, strNames)
            Else
                'Close current game form
                closeGameForm = True
                If My.Settings.gameSize = "hires" Then
                    frmHiResGame.Close()
                Else
                    frmLowResGame.Close()
                End If
                closeGameForm = False

                'Show main menu
                frmMainMenu.Show()
            End If
        End If
    End Sub

    Public Sub rollDice(Optional ByVal resetDice As Boolean = False, Optional ByVal rollYahtzee As Boolean = False)
        If resetDice Then
            'Reset dice array to blank
            For i As Integer = 0 To 4
                d(i).Value = 0
                d(i).Keep = False
            Next
        End If

        'Start dice animation
        If My.Settings.gameSize = "hires" Then
            frmHiResGame.tmrRollAni.Enabled = True
        Else
            frmLowResGame.tmrRollAni.Enabled = True
        End If

        'Set dice values
        For i As Integer = 0 To 4
            If d(i).Keep = False Then
                Randomize()
                d(i).Value = randomValue(1, 6)
            End If
        Next

        'Roll a yahtzee - used for testing
        If rollYahtzee = True Then
            Randomize()
            Dim val As Integer = randomValue(1, 6)
            For i As Integer = 0 To 4
                d(i).Value = val
            Next
        End If

        'Set the dice value array
        setDiceArray()

        'Increase player's rolls by 1
        p(intCP).Rolls += 1

        'Update roll status - button and number of dice rolls completed
        If My.Settings.gameSize = "hires" Then
            frmHiResGame.setRollStatus()
        Else
            frmLowResGame.setRollStatus()
        End If
    End Sub

    Public Function getCP() As Integer
        'Retrieves current player
        For i As Integer = 0 To p.Length - 1
            If p(i).Turn = True Then
                getCP = i
                Exit For
            End If
        Next
        Return getCP
    End Function

    Public Sub setDiceArray()
        'Set the dice value array - used to calculate scores
        For i As Integer = 0 To 4
            intDiceVal(i) = d(i).Value
        Next
        Array.Sort(intDiceVal)
    End Sub

    Public Function randomValue(ByVal Lowerbound As Integer, ByVal Upperbound As Integer) As Integer
        'Returns a random number
        Return Int((Upperbound - Lowerbound + 1) * Rnd() + Lowerbound)
    End Function

    Public Function addSameDice(ByVal intDieNumber As Integer) As Integer
        'Adds all dice with a particular value
        'Eg. if intDieNumber = 1, will add up how many 1s there are
        addSameDice = 0
        For i As Integer = 0 To 4
            If intDiceVal(i) = intDieNumber Then
                addSameDice += intDieNumber
            End If
        Next
        Return addSameDice
    End Function

    Public Function calcChance() As Integer
        'Sum of all dice values
        calcChance = 0
        If checkYahtzee() And checkUpperSlot(intDiceVal(0)) = False Then
            'Player has a Yahtzee, but relevant upper section scorebox is not filled
            calcChance = 0
        Else
            For i As Integer = 0 To 4
                calcChance += intDiceVal(i)
            Next
        End If
        Return calcChance
    End Function

    Public Function calcSmlStr() As Integer
        'Calculates score for small straight scorebox
        calcSmlStr = 0

        'Sort dice value array - move duplicates to end
        For i As Integer = 0 To 3
            If intDiceVal(i) = intDiceVal(i + 1) Then
                For j As Integer = i To 3
                    Dim temp As Integer = intDiceVal(j)
                    intDiceVal(j) = intDiceVal(j + 1)
                    intDiceVal(j + 1) = temp
                Next
            End If
        Next

        'Get score for scorebox
        If (intDiceVal(0) = 1 And intDiceVal(1) = 2 And intDiceVal(2) = 3 And intDiceVal(3) = 4) Or _
            (intDiceVal(1) = 1 And intDiceVal(2) = 2 And intDiceVal(3) = 3 And intDiceVal(4) = 4) Or _
            (intDiceVal(0) = 2 And intDiceVal(1) = 3 And intDiceVal(2) = 4 And intDiceVal(3) = 5) Or _
            (intDiceVal(1) = 2 And intDiceVal(2) = 3 And intDiceVal(3) = 4 And intDiceVal(4) = 5) Or _
            (intDiceVal(0) = 3 And intDiceVal(1) = 4 And intDiceVal(2) = 5 And intDiceVal(3) = 6) Or _
            (intDiceVal(1) = 3 And intDiceVal(2) = 4 And intDiceVal(3) = 5 And intDiceVal(4) = 6) Then
            'Player has small straight
            calcSmlStr = 30
        ElseIf checkYahtzee() And checkUpperSlot(intDiceVal(0)) = True Then
            'Player has Yahtzee and relevant upper section scorebox is filled
            calcSmlStr = 30
        End If

        'Reset the dice values array - sort ascending
        setDiceArray()
        Return calcSmlStr
    End Function

    Public Function calcLrgStr() As Integer
        'Gets value for long straight scorebox
        calcLrgStr = 0

        'Get score for scorebox
        If intDiceVal(0) = 1 And intDiceVal(1) = 2 And intDiceVal(2) = 3 And intDiceVal(3) = 4 And intDiceVal(4) = 5 Then
            'Player has long straight
            calcLrgStr = 40
        ElseIf intDiceVal(0) = 2 And intDiceVal(1) = 3 And intDiceVal(2) = 4 And intDiceVal(3) = 5 And intDiceVal(4) = 6 Then
            'Player has long straight
            calcLrgStr = 40
        ElseIf checkYahtzee() And checkUpperSlot(intDiceVal(0)) = True Then
            'Player has Yahtzee and relevant upper section scorebox is filled
            calcLrgStr = 40
        End If
        Return calcLrgStr
    End Function

    Public Function calc3Kind() As Integer
        'Gets value for 3 of a Kind scorebox
        calc3Kind = 0
        If intDiceVal(0) = intDiceVal(1) And intDiceVal(0) = intDiceVal(2) Then
            'Player has 3 of a Kind
            calc3Kind = calcChance()
        ElseIf intDiceVal(1) = intDiceVal(2) And intDiceVal(1) = intDiceVal(3) Then
            'Player has 3 of a Kind
            calc3Kind = calcChance()
        ElseIf intDiceVal(2) = intDiceVal(3) And intDiceVal(2) = intDiceVal(4) Then
            'Player has 3 of a Kind
            calc3Kind = calcChance()
        ElseIf checkYahtzee() And checkUpperSlot(intDiceVal(0)) = True Then
            'Player has Yahtzee and relevant upper section scorebox is filled
            calc3Kind = calcChance()
        End If
        Return calc3Kind
    End Function

    Public Function calc4Kind() As Integer
        'Gets value for 4 of a Kind scorebox
        calc4Kind = 0
        If intDiceVal(0) = intDiceVal(1) And intDiceVal(0) = intDiceVal(2) And intDiceVal(0) = intDiceVal(3) Then
            'Player has 4 of a Kind
            calc4Kind = calcChance()
        ElseIf intDiceVal(1) = intDiceVal(2) And intDiceVal(1) = intDiceVal(3) And intDiceVal(1) = intDiceVal(4) Then
            'Player has 4 of a Kind
            calc4Kind = calcChance()
        ElseIf checkYahtzee() And checkUpperSlot(intDiceVal(0)) = True Then
            'Player has Yahtzee and relevant upper section scorebox is filled
            calc4Kind = calcChance()
        End If
        Return calc4Kind
    End Function

    Public Function calcFullHouse() As Integer
        'Gets value for full house scorebox
        calcFullHouse = 0
        If intDiceVal(0) = intDiceVal(1) And intDiceVal(2) = intDiceVal(3) And intDiceVal(2) = intDiceVal(4) And Not intDiceVal(1) = intDiceVal(2) Then
            'Player has a full house
            calcFullHouse = 25
        ElseIf intDiceVal(0) = intDiceVal(1) And intDiceVal(0) = intDiceVal(2) And intDiceVal(3) = intDiceVal(4) And Not intDiceVal(2) = intDiceVal(3) Then
            'Player has a full house
            calcFullHouse = 25
        ElseIf checkYahtzee() And checkUpperSlot(intDiceVal(0)) = True Then
            'Player has Yahtzee and relevant upper section scorebox is filled
            calcFullHouse = 25
        End If
        Return calcFullHouse
    End Function

    Public Function calcYahtzee() As Integer
        'Gets value for Yahtzee scorebox
        If checkYahtzee() Then
            calcYahtzee = 50
        Else
            calcYahtzee = 0
        End If
        Return calcYahtzee
    End Function

    Public Function checkYahtzee() As Boolean
        'Checks if the current dice values are a Yahtzee (5 of a kind)
        If intDiceVal(0) = intDiceVal(1) And intDiceVal(0) = intDiceVal(2) And intDiceVal(0) = intDiceVal(3) And intDiceVal(0) = intDiceVal(4) Then
            'Player has a Yahtzee
            checkYahtzee = True
        Else
            'Player doesn't have a Yahtzee
            checkYahtzee = False
        End If
        Return checkYahtzee
    End Function

    Public Function calcYahtzeeBonus(ByVal storeYahtzeeBonus As Boolean, Optional ByVal tempBonusCalc As Boolean = False) As Integer
        'Calculates the Yahtzee bonus for the current dice values and player's scorebox values
        If checkYahtzee() Then
            'Player has a Yahtzee, so calculate bonus
            If p(intCP).Yahtzee.Filled = False Then
                'Yahtzee box is empty
                calcYahtzeeBonus = 0
            ElseIf p(intCP).Yahtzee.Filled = True And p(intCP).Yahtzee.Score = 0 Then
                'Yahtzee box is filled with score of 0
                calcYahtzeeBonus = 0
            ElseIf tempBonusCalc = True Then
                'Player has a Yahtzee, and has 50 in Yahtzee box, so return temporary score
                'Temporary score is 100 points higher than current Yahtzee bonus
                'Only used for mouse hover
                calcYahtzeeBonus = p(intCP).numYahtzeeBonus * 100 + 100
            Else
                If storeYahtzeeBonus = True Then
                    'Store the bonus
                    p(intCP).numYahtzeeBonus += 1
                End If
                'Return current bonus value
                calcYahtzeeBonus = p(intCP).numYahtzeeBonus * 100
            End If
        Else
            'Player doesn't have a Yahtzee
            calcYahtzeeBonus = p(intCP).numYahtzeeBonus * 100
        End If
    End Function

    Public Function checkUpperSlot(ByVal upperSlot As Integer) As Boolean
        'Checks if upper slot is filled or not
        'Returns False if slot is not filled
        'Returns True if slot is filled
        checkUpperSlot = False
        Select Case upperSlot
            Case 1
                If p(intCP).Aces.Filled = True Then
                    checkUpperSlot = True
                End If
            Case 2
                If p(intCP).Twos.Filled = True Then
                    checkUpperSlot = True
                End If
            Case 3
                If p(intCP).Threes.Filled = True Then
                    checkUpperSlot = True
                End If
            Case 4
                If p(intCP).Fours.Filled = True Then
                    checkUpperSlot = True
                End If
            Case 5
                If p(intCP).Fives.Filled = True Then
                    checkUpperSlot = True
                End If
            Case 6
                If p(intCP).Sixes.Filled = True Then
                    checkUpperSlot = True
                End If
        End Select
        Return checkUpperSlot
    End Function

    Public Function filledScoreboxes(ByVal player As Integer) As Integer
        'Counts how many scoreboxes for a player are filler
        Dim filled As Integer = 0
        If p(player).Aces.Filled = True Then
            filled += 1
        End If
        If p(player).Twos.Filled = True Then
            filled += 1
        End If
        If p(player).Threes.Filled = True Then
            filled += 1
        End If
        If p(player).Fours.Filled = True Then
            filled += 1
        End If
        If p(player).Fives.Filled = True Then
            filled += 1
        End If
        If p(player).Sixes.Filled = True Then
            filled += 1
        End If
        If p(player).ThreeKind.Filled = True Then
            filled += 1
        End If
        If p(player).FourKind.Filled = True Then
            filled += 1
        End If
        If p(player).FullHouse.Filled = True Then
            filled += 1
        End If
        If p(player).SmallStraight.Filled = True Then
            filled += 1
        End If
        If p(player).LargeStraight.Filled = True Then
            filled += 1
        End If
        If p(player).Yahtzee.Filled = True Then
            filled += 1
        End If
        If p(player).Chance.Filled = True Then
            filled += 1
        End If
        Return filled
    End Function
End Module