﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Yahtzee")> 
<Assembly: AssemblyDescription("Yahtzee is a poker-dice game. It can be played by up to 4 players, of age 4+. Based on the board game by Hasbro. Develped by Joel Addison, for MicroHawk Technology Inc.")> 
<Assembly: AssemblyCompany("MicroHawk Technology Inc")> 
<Assembly: AssemblyProduct("Yahtzee")> 
<Assembly: AssemblyCopyright("Copyright © Joel Addison 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("5aa9cc99-5fd1-44e4-8cfd-9c6c9955837d")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
