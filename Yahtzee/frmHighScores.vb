﻿Public Class frmHighScores

    Private Sub HighScores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Show high scores without yahtzees
        showHS()

        'Set help file location
        hlpYahtzee.HelpNamespace = My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm"
    End Sub

    Private Sub rbWithYaht_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbWithYaht.CheckedChanged
        'Show high scores without yahtzees
        showHS()
    End Sub

    Private Sub rbNoYaht_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbNoYaht.CheckedChanged
        'Show high scores with yahtzees
        showHS(True)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        'Confirm user wants to delete all high scores
        If MessageBox.Show("Are you sure you want to clear all High Scores?" & vbNewLine & _
                           "Scores with and without Yahtzees will be removed.", My.Application.Info.Title, _
                           MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            clearHS()
            showHS()
        End If
    End Sub

    Private Sub showHS(Optional ByVal withoutYahtzees As Boolean = False)
        'loads high scores with or without yahtzees from file
        loadHS(withoutYahtzees)

        'Clear listview
        lsvHighScores.Items.Clear()

        If hs.Count = 0 Then 'No high scores
            'Show 'no high scores' message
            lblNoScores.Visible = True
            lsvHighScores.Enabled = False
        Else 'High scores exist
            'Hide 'no high scores' message
            lblNoScores.Visible = False
            lsvHighScores.Enabled = True

            'list high scores in listview
            Dim str(4) As String
            Dim itm As ListViewItem
            For i As Integer = 0 To hs.Count - 1
                Dim tempHS As New HighScore
                tempHS = hs(i)
                str(0) = i + 1
                str(1) = tempHS.Name
                str(2) = tempHS.Score
                str(3) = tempHS.numYahtzees
                str(4) = FormatDateTime(tempHS.gameDate, DateFormat.ShortDate)
                itm = New ListViewItem(str)
                lsvHighScores.Items.Add(itm)
            Next
        End If
    End Sub
End Class