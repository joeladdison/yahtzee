﻿Public Class frmOptions

    Private Sub frmOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Load current settings
        'Number of random dice to show for roll animation
        nudDiceRoll.Value = CDec(My.Settings.numAniDiceRolls)

        'Max number of high scores to save
        nudHighScores.Value = CDec(My.Settings.maxHighScores)

        'Show confirmation message before exiting program
        cbConfirmWinClose.Checked = My.Settings.exitConfirm

        'Show background image on game screen
        cbShowBgImg.Checked = My.Settings.showBgImage

        'Size of game screen to use
        If My.Settings.gameSize = "hires" Then
            rbHiRes.Checked = True
        ElseIf My.Settings.gameSize = "lowres" Then
            rbLowRes.Checked = True
        End If

        'Disable game screen size option if game form is open
        If gameRunning() = True Then
            GroupBox1.Enabled = False
        End If

        'Set help file location
        hlpYahtzee.HelpNamespace = My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm"
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Save new settings
        With My.Settings
            'Max number of high scores to save
            .maxHighScores = nudHighScores.Value

            'Number of random dice to show for roll animation
            .numAniDiceRolls = nudDiceRoll.Value

            'Show confirmation message before exiting program
            .exitConfirm = cbConfirmWinClose.Checked

            'Show background image on column layout game screen
            .showBgImage = cbShowBgImg.Checked

            'Only save game screen size option if no game is in progress
            If gameRunning() = False Then
                'Size of game screen to use
                If rbHiRes.Checked = True Then
                    .gameSize = "hires"
                Else
                    .gameSize = "lowres"
                End If
            End If
        End With

        'Save settings and close form
        My.Settings.Save()
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Restore to previous settings
        My.Settings.Reload()
        Me.Close()
    End Sub

    Private Sub btnDefaults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefaults.Click
        'Restore to default settings
        'Max number of high scores to save
        nudDiceRoll.Value = 15

        'Number of random dice to show for roll animation
        nudHighScores.Value = 10

        'Show confirmation message before exiting program
        cbConfirmWinClose.Checked = True

        'Show background image on game screen
        cbShowBgImg.Checked = True

        'Only reset game screen size option if no game is in progress
        If gameRunning() = False Then
            'Size of game screen to use
            If gameSize() = "hires" Then
                rbHiRes.Checked = True
            Else
                rbLowRes.Checked = True
            End If
        End If
    End Sub

    Public Function gameSize() As String
        'Gets height (resolution) of screen, and recommends game screen to use
        Dim intHeight As Integer = Screen.PrimaryScreen.Bounds.Height

        If intHeight <= 768 Then
            'recommend Low-Res game screen
            gameSize = "lowres"
        Else
            'recommend High-Res game screen
            gameSize = "hires"
        End If
    End Function

    Private Function gameRunning() As Boolean
        'Returns true if game form is open
        gameRunning = False

        'Check if game form is currently open
        Dim f As Form
        For Each f In My.Application.OpenForms
            If f.Name = "frmHiResGame" Or f.Name = "frmLowResGame" Then
                gameRunning = True
                Exit For
            End If
        Next
    End Function
End Class