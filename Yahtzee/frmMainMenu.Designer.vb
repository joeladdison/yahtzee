﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMainMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMainMenu))
        Me.btnLoad = New System.Windows.Forms.Button
        Me.btnHighScores = New System.Windows.Forms.Button
        Me.btnHelp = New System.Windows.Forms.Button
        Me.btnRules = New System.Windows.Forms.Button
        Me.btnOptions = New System.Windows.Forms.Button
        Me.btnAbout = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.grpNewGame = New System.Windows.Forms.GroupBox
        Me.btnStart = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.rbFourPlayers = New System.Windows.Forms.RadioButton
        Me.rbThreePlayers = New System.Windows.Forms.RadioButton
        Me.rbTwoPlayers = New System.Windows.Forms.RadioButton
        Me.rbOnePlayer = New System.Windows.Forms.RadioButton
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.txtPlayer4 = New Yahtzee.WaterMarkTextBox
        Me.txtPlayer3 = New Yahtzee.WaterMarkTextBox
        Me.txtPlayer2 = New Yahtzee.WaterMarkTextBox
        Me.txtPlayer1 = New Yahtzee.WaterMarkTextBox
        Me.hlpYahtzee = New System.Windows.Forms.HelpProvider
        Me.grpNewGame.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(12, 107)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(156, 23)
        Me.btnLoad.TabIndex = 59
        Me.btnLoad.Text = "Load Game"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'btnHighScores
        '
        Me.btnHighScores.Location = New System.Drawing.Point(12, 136)
        Me.btnHighScores.Name = "btnHighScores"
        Me.btnHighScores.Size = New System.Drawing.Size(156, 23)
        Me.btnHighScores.TabIndex = 60
        Me.btnHighScores.Text = "High Scores"
        Me.btnHighScores.UseVisualStyleBackColor = True
        '
        'btnHelp
        '
        Me.btnHelp.Location = New System.Drawing.Point(12, 165)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(156, 23)
        Me.btnHelp.TabIndex = 61
        Me.btnHelp.Text = "How to Play"
        Me.btnHelp.UseVisualStyleBackColor = True
        '
        'btnRules
        '
        Me.btnRules.Location = New System.Drawing.Point(12, 194)
        Me.btnRules.Name = "btnRules"
        Me.btnRules.Size = New System.Drawing.Size(156, 23)
        Me.btnRules.TabIndex = 62
        Me.btnRules.Text = "Yahtzee Rules"
        Me.btnRules.UseVisualStyleBackColor = True
        '
        'btnOptions
        '
        Me.btnOptions.Location = New System.Drawing.Point(12, 223)
        Me.btnOptions.Name = "btnOptions"
        Me.btnOptions.Size = New System.Drawing.Size(156, 23)
        Me.btnOptions.TabIndex = 63
        Me.btnOptions.Text = "Options"
        Me.btnOptions.UseVisualStyleBackColor = True
        '
        'btnAbout
        '
        Me.btnAbout.Location = New System.Drawing.Point(12, 252)
        Me.btnAbout.Name = "btnAbout"
        Me.btnAbout.Size = New System.Drawing.Size(75, 23)
        Me.btnAbout.TabIndex = 64
        Me.btnAbout.Text = "About"
        Me.btnAbout.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(93, 252)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 65
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'grpNewGame
        '
        Me.grpNewGame.Controls.Add(Me.txtPlayer4)
        Me.grpNewGame.Controls.Add(Me.txtPlayer3)
        Me.grpNewGame.Controls.Add(Me.txtPlayer2)
        Me.grpNewGame.Controls.Add(Me.txtPlayer1)
        Me.grpNewGame.Controls.Add(Me.btnStart)
        Me.grpNewGame.Controls.Add(Me.Label2)
        Me.grpNewGame.Controls.Add(Me.Label1)
        Me.grpNewGame.Controls.Add(Me.rbFourPlayers)
        Me.grpNewGame.Controls.Add(Me.rbThreePlayers)
        Me.grpNewGame.Controls.Add(Me.rbTwoPlayers)
        Me.grpNewGame.Controls.Add(Me.rbOnePlayer)
        Me.grpNewGame.Location = New System.Drawing.Point(174, 107)
        Me.grpNewGame.Name = "grpNewGame"
        Me.grpNewGame.Size = New System.Drawing.Size(243, 168)
        Me.grpNewGame.TabIndex = 66
        Me.grpNewGame.TabStop = False
        Me.grpNewGame.Text = "New Game"
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(75, 139)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(75, 23)
        Me.btnStart.TabIndex = 10
        Me.btnStart.Text = "Start Game"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "# of Players"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(105, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Player Names"
        '
        'rbFourPlayers
        '
        Me.rbFourPlayers.AutoSize = True
        Me.rbFourPlayers.Location = New System.Drawing.Point(16, 115)
        Me.rbFourPlayers.Name = "rbFourPlayers"
        Me.rbFourPlayers.Size = New System.Drawing.Size(68, 17)
        Me.rbFourPlayers.TabIndex = 3
        Me.rbFourPlayers.TabStop = True
        Me.rbFourPlayers.Text = "4 Players"
        Me.rbFourPlayers.UseVisualStyleBackColor = True
        '
        'rbThreePlayers
        '
        Me.rbThreePlayers.AutoSize = True
        Me.rbThreePlayers.Location = New System.Drawing.Point(16, 89)
        Me.rbThreePlayers.Name = "rbThreePlayers"
        Me.rbThreePlayers.Size = New System.Drawing.Size(68, 17)
        Me.rbThreePlayers.TabIndex = 2
        Me.rbThreePlayers.TabStop = True
        Me.rbThreePlayers.Text = "3 Players"
        Me.rbThreePlayers.UseVisualStyleBackColor = True
        '
        'rbTwoPlayers
        '
        Me.rbTwoPlayers.AutoSize = True
        Me.rbTwoPlayers.Location = New System.Drawing.Point(16, 63)
        Me.rbTwoPlayers.Name = "rbTwoPlayers"
        Me.rbTwoPlayers.Size = New System.Drawing.Size(68, 17)
        Me.rbTwoPlayers.TabIndex = 1
        Me.rbTwoPlayers.TabStop = True
        Me.rbTwoPlayers.Text = "2 Players"
        Me.rbTwoPlayers.UseVisualStyleBackColor = True
        '
        'rbOnePlayer
        '
        Me.rbOnePlayer.AutoSize = True
        Me.rbOnePlayer.Checked = True
        Me.rbOnePlayer.Location = New System.Drawing.Point(16, 36)
        Me.rbOnePlayer.Name = "rbOnePlayer"
        Me.rbOnePlayer.Size = New System.Drawing.Size(63, 17)
        Me.rbOnePlayer.TabIndex = 0
        Me.rbOnePlayer.TabStop = True
        Me.rbOnePlayer.Text = "1 Player"
        Me.rbOnePlayer.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(122, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(187, 86)
        Me.PictureBox1.TabIndex = 67
        Me.PictureBox1.TabStop = False
        '
        'txtPlayer4
        '
        Me.txtPlayer4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtPlayer4.Location = New System.Drawing.Point(105, 114)
        Me.txtPlayer4.MaxLength = 15
        Me.txtPlayer4.Name = "txtPlayer4"
        Me.txtPlayer4.Size = New System.Drawing.Size(123, 20)
        Me.txtPlayer4.TabIndex = 7
        Me.txtPlayer4.WaterMarkColor = System.Drawing.Color.Gray
        Me.txtPlayer4.WaterMarkText = "Player 4"
        '
        'txtPlayer3
        '
        Me.txtPlayer3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtPlayer3.Location = New System.Drawing.Point(105, 88)
        Me.txtPlayer3.MaxLength = 15
        Me.txtPlayer3.Name = "txtPlayer3"
        Me.txtPlayer3.Size = New System.Drawing.Size(123, 20)
        Me.txtPlayer3.TabIndex = 6
        Me.txtPlayer3.WaterMarkColor = System.Drawing.Color.Gray
        Me.txtPlayer3.WaterMarkText = "Player 3"
        '
        'txtPlayer2
        '
        Me.txtPlayer2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtPlayer2.Location = New System.Drawing.Point(105, 62)
        Me.txtPlayer2.MaxLength = 15
        Me.txtPlayer2.Name = "txtPlayer2"
        Me.txtPlayer2.Size = New System.Drawing.Size(123, 20)
        Me.txtPlayer2.TabIndex = 5
        Me.txtPlayer2.WaterMarkColor = System.Drawing.Color.Gray
        Me.txtPlayer2.WaterMarkText = "Player 2"
        '
        'txtPlayer1
        '
        Me.txtPlayer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtPlayer1.Location = New System.Drawing.Point(105, 36)
        Me.txtPlayer1.MaxLength = 15
        Me.txtPlayer1.Name = "txtPlayer1"
        Me.txtPlayer1.Size = New System.Drawing.Size(123, 20)
        Me.txtPlayer1.TabIndex = 4
        Me.txtPlayer1.WaterMarkColor = System.Drawing.Color.Gray
        Me.txtPlayer1.WaterMarkText = "Player 1"
        '
        'frmMainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(431, 287)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.grpNewGame)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnAbout)
        Me.Controls.Add(Me.btnOptions)
        Me.Controls.Add(Me.btnRules)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnHighScores)
        Me.Controls.Add(Me.btnLoad)
        Me.hlpYahtzee.SetHelpKeyword(Me, "110")
        Me.hlpYahtzee.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.TopicId)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(447, 325)
        Me.MinimumSize = New System.Drawing.Size(447, 325)
        Me.Name = "frmMainMenu"
        Me.hlpYahtzee.SetShowHelp(Me, True)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Yahtzee - Main Menu"
        Me.grpNewGame.ResumeLayout(False)
        Me.grpNewGame.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents btnHighScores As System.Windows.Forms.Button
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents btnRules As System.Windows.Forms.Button
    Friend WithEvents btnOptions As System.Windows.Forms.Button
    Friend WithEvents btnAbout As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents grpNewGame As System.Windows.Forms.GroupBox
    Friend WithEvents rbFourPlayers As System.Windows.Forms.RadioButton
    Friend WithEvents rbThreePlayers As System.Windows.Forms.RadioButton
    Friend WithEvents rbTwoPlayers As System.Windows.Forms.RadioButton
    Friend WithEvents rbOnePlayer As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnStart As System.Windows.Forms.Button
    Friend WithEvents txtPlayer1 As Yahtzee.WaterMarkTextBox
    Friend WithEvents txtPlayer4 As Yahtzee.WaterMarkTextBox
    Friend WithEvents txtPlayer3 As Yahtzee.WaterMarkTextBox
    Friend WithEvents txtPlayer2 As Yahtzee.WaterMarkTextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents hlpYahtzee As System.Windows.Forms.HelpProvider
End Class
