﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHiResGame
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHiResGame))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NewGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.SaveGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LoadGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.HighScoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.YahtzeeHelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.YahtzeeRulesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.tmrRollAni = New System.Windows.Forms.Timer(Me.components)
        Me.lblChance = New System.Windows.Forms.Label
        Me.lblYahtzee = New System.Windows.Forms.Label
        Me.btnYahtzee = New System.Windows.Forms.Button
        Me.lblLargeStraight = New System.Windows.Forms.Label
        Me.lblSmallStraight = New System.Windows.Forms.Label
        Me.lblFullHouse = New System.Windows.Forms.Label
        Me.lbl4ofKind = New System.Windows.Forms.Label
        Me.lblSixes = New System.Windows.Forms.Label
        Me.lbl3ofKind = New System.Windows.Forms.Label
        Me.lblFives = New System.Windows.Forms.Label
        Me.lblFours = New System.Windows.Forms.Label
        Me.lblThrees = New System.Windows.Forms.Label
        Me.lblTwos = New System.Windows.Forms.Label
        Me.lblAces = New System.Windows.Forms.Label
        Me.lblYahtzeeBonus = New System.Windows.Forms.Label
        Me.lblGrandTot = New System.Windows.Forms.Label
        Me.lblLowerTot = New System.Windows.Forms.Label
        Me.lblBonus = New System.Windows.Forms.Label
        Me.lblUpperTot = New System.Windows.Forms.Label
        Me.lblRolls = New System.Windows.Forms.Label
        Me.btnRoll = New System.Windows.Forms.Button
        Me.pctLogo = New System.Windows.Forms.PictureBox
        Me.hlpYahtzee = New System.Windows.Forms.HelpProvider
        Me.MenuStrip1.SuspendLayout()
        CType(Me.pctLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(394, 24)
        Me.MenuStrip1.TabIndex = 55
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewGameToolStripMenuItem, Me.ToolStripSeparator2, Me.SaveGameToolStripMenuItem, Me.LoadGameToolStripMenuItem, Me.ToolStripSeparator1, Me.HighScoresToolStripMenuItem, Me.ToolStripSeparator4, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'NewGameToolStripMenuItem
        '
        Me.NewGameToolStripMenuItem.Name = "NewGameToolStripMenuItem"
        Me.NewGameToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.NewGameToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.NewGameToolStripMenuItem.Text = "&New Game"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(172, 6)
        '
        'SaveGameToolStripMenuItem
        '
        Me.SaveGameToolStripMenuItem.Name = "SaveGameToolStripMenuItem"
        Me.SaveGameToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveGameToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.SaveGameToolStripMenuItem.Text = "&Save Game"
        '
        'LoadGameToolStripMenuItem
        '
        Me.LoadGameToolStripMenuItem.Name = "LoadGameToolStripMenuItem"
        Me.LoadGameToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.LoadGameToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.LoadGameToolStripMenuItem.Text = "&Load Game"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(172, 6)
        '
        'HighScoresToolStripMenuItem
        '
        Me.HighScoresToolStripMenuItem.Name = "HighScoresToolStripMenuItem"
        Me.HighScoresToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.HighScoresToolStripMenuItem.Text = "&High Scores"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(172, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.ShortcutKeyDisplayString = ""
        Me.ExitToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OptionsToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.ToolsToolStripMenuItem.Text = "&Tools"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.OptionsToolStripMenuItem.Text = "&Options"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.YahtzeeHelpToolStripMenuItem, Me.YahtzeeRulesToolStripMenuItem, Me.ToolStripSeparator3, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "&Help"
        '
        'YahtzeeHelpToolStripMenuItem
        '
        Me.YahtzeeHelpToolStripMenuItem.Name = "YahtzeeHelpToolStripMenuItem"
        Me.YahtzeeHelpToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.YahtzeeHelpToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.YahtzeeHelpToolStripMenuItem.Text = "Yahtzee &Help"
        '
        'YahtzeeRulesToolStripMenuItem
        '
        Me.YahtzeeRulesToolStripMenuItem.Name = "YahtzeeRulesToolStripMenuItem"
        Me.YahtzeeRulesToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F1), System.Windows.Forms.Keys)
        Me.YahtzeeRulesToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.YahtzeeRulesToolStripMenuItem.Text = "Yahtzee &Rules"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(189, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.AboutToolStripMenuItem.Text = "&About Yahtzee"
        '
        'tmrRollAni
        '
        Me.tmrRollAni.Interval = 75
        '
        'lblChance
        '
        Me.lblChance.AutoSize = True
        Me.lblChance.Location = New System.Drawing.Point(192, 558)
        Me.lblChance.Name = "lblChance"
        Me.lblChance.Size = New System.Drawing.Size(44, 13)
        Me.lblChance.TabIndex = 115
        Me.lblChance.Text = "Chance"
        '
        'lblYahtzee
        '
        Me.lblYahtzee.AutoSize = True
        Me.lblYahtzee.Location = New System.Drawing.Point(190, 529)
        Me.lblYahtzee.Name = "lblYahtzee"
        Me.lblYahtzee.Size = New System.Drawing.Size(46, 13)
        Me.lblYahtzee.TabIndex = 114
        Me.lblYahtzee.Text = "Yahtzee"
        '
        'btnYahtzee
        '
        Me.btnYahtzee.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYahtzee.Location = New System.Drawing.Point(108, 614)
        Me.btnYahtzee.Name = "btnYahtzee"
        Me.btnYahtzee.Size = New System.Drawing.Size(48, 28)
        Me.btnYahtzee.TabIndex = 116
        Me.btnYahtzee.Text = "Yahtzee"
        Me.btnYahtzee.UseVisualStyleBackColor = True
        Me.btnYahtzee.Visible = False
        '
        'lblLargeStraight
        '
        Me.lblLargeStraight.AutoSize = True
        Me.lblLargeStraight.Location = New System.Drawing.Point(163, 500)
        Me.lblLargeStraight.Name = "lblLargeStraight"
        Me.lblLargeStraight.Size = New System.Drawing.Size(73, 13)
        Me.lblLargeStraight.TabIndex = 113
        Me.lblLargeStraight.Text = "Large Straight"
        '
        'lblSmallStraight
        '
        Me.lblSmallStraight.AutoSize = True
        Me.lblSmallStraight.Location = New System.Drawing.Point(165, 471)
        Me.lblSmallStraight.Name = "lblSmallStraight"
        Me.lblSmallStraight.Size = New System.Drawing.Size(71, 13)
        Me.lblSmallStraight.TabIndex = 112
        Me.lblSmallStraight.Text = "Small Straight"
        '
        'lblFullHouse
        '
        Me.lblFullHouse.AutoSize = True
        Me.lblFullHouse.Location = New System.Drawing.Point(179, 442)
        Me.lblFullHouse.Name = "lblFullHouse"
        Me.lblFullHouse.Size = New System.Drawing.Size(57, 13)
        Me.lblFullHouse.TabIndex = 111
        Me.lblFullHouse.Text = "Full House"
        '
        'lbl4ofKind
        '
        Me.lbl4ofKind.AutoSize = True
        Me.lbl4ofKind.Location = New System.Drawing.Point(163, 413)
        Me.lbl4ofKind.Name = "lbl4ofKind"
        Me.lbl4ofKind.Size = New System.Drawing.Size(73, 13)
        Me.lbl4ofKind.TabIndex = 110
        Me.lbl4ofKind.Text = "Four of a Kind"
        '
        'lblSixes
        '
        Me.lblSixes.AutoSize = True
        Me.lblSixes.Location = New System.Drawing.Point(189, 297)
        Me.lblSixes.Name = "lblSixes"
        Me.lblSixes.Size = New System.Drawing.Size(47, 13)
        Me.lblSixes.TabIndex = 108
        Me.lblSixes.Text = "Sixes (6)"
        '
        'lbl3ofKind
        '
        Me.lbl3ofKind.AutoSize = True
        Me.lbl3ofKind.Location = New System.Drawing.Point(156, 384)
        Me.lbl3ofKind.Name = "lbl3ofKind"
        Me.lbl3ofKind.Size = New System.Drawing.Size(80, 13)
        Me.lbl3ofKind.TabIndex = 109
        Me.lbl3ofKind.Text = "Three of a Kind"
        '
        'lblFives
        '
        Me.lblFives.AutoSize = True
        Me.lblFives.Location = New System.Drawing.Point(189, 268)
        Me.lblFives.Name = "lblFives"
        Me.lblFives.Size = New System.Drawing.Size(47, 13)
        Me.lblFives.TabIndex = 107
        Me.lblFives.Text = "Fives (5)"
        '
        'lblFours
        '
        Me.lblFours.AutoSize = True
        Me.lblFours.Location = New System.Drawing.Point(188, 239)
        Me.lblFours.Name = "lblFours"
        Me.lblFours.Size = New System.Drawing.Size(48, 13)
        Me.lblFours.TabIndex = 106
        Me.lblFours.Text = "Fours (4)"
        '
        'lblThrees
        '
        Me.lblThrees.AutoSize = True
        Me.lblThrees.Location = New System.Drawing.Point(181, 210)
        Me.lblThrees.Name = "lblThrees"
        Me.lblThrees.Size = New System.Drawing.Size(55, 13)
        Me.lblThrees.TabIndex = 105
        Me.lblThrees.Text = "Threes (3)"
        '
        'lblTwos
        '
        Me.lblTwos.AutoSize = True
        Me.lblTwos.Location = New System.Drawing.Point(188, 181)
        Me.lblTwos.Name = "lblTwos"
        Me.lblTwos.Size = New System.Drawing.Size(48, 13)
        Me.lblTwos.TabIndex = 104
        Me.lblTwos.Text = "Twos (2)"
        '
        'lblAces
        '
        Me.lblAces.AutoSize = True
        Me.lblAces.Location = New System.Drawing.Point(190, 152)
        Me.lblAces.Name = "lblAces"
        Me.lblAces.Size = New System.Drawing.Size(46, 13)
        Me.lblAces.TabIndex = 103
        Me.lblAces.Text = "Aces (1)"
        '
        'lblYahtzeeBonus
        '
        Me.lblYahtzeeBonus.AutoSize = True
        Me.lblYahtzeeBonus.Location = New System.Drawing.Point(157, 587)
        Me.lblYahtzeeBonus.Name = "lblYahtzeeBonus"
        Me.lblYahtzeeBonus.Size = New System.Drawing.Size(79, 13)
        Me.lblYahtzeeBonus.TabIndex = 100
        Me.lblYahtzeeBonus.Text = "Yahtzee Bonus"
        Me.lblYahtzeeBonus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblGrandTot
        '
        Me.lblGrandTot.AutoSize = True
        Me.lblGrandTot.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrandTot.Location = New System.Drawing.Point(162, 645)
        Me.lblGrandTot.Name = "lblGrandTot"
        Me.lblGrandTot.Size = New System.Drawing.Size(74, 13)
        Me.lblGrandTot.TabIndex = 102
        Me.lblGrandTot.Text = "Grand Total"
        Me.lblGrandTot.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLowerTot
        '
        Me.lblLowerTot.AutoSize = True
        Me.lblLowerTot.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLowerTot.Location = New System.Drawing.Point(162, 616)
        Me.lblLowerTot.Name = "lblLowerTot"
        Me.lblLowerTot.Size = New System.Drawing.Size(74, 13)
        Me.lblLowerTot.TabIndex = 101
        Me.lblLowerTot.Text = "Lower Total"
        Me.lblLowerTot.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBonus
        '
        Me.lblBonus.AutoSize = True
        Me.lblBonus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBonus.Location = New System.Drawing.Point(199, 326)
        Me.lblBonus.Name = "lblBonus"
        Me.lblBonus.Size = New System.Drawing.Size(37, 13)
        Me.lblBonus.TabIndex = 98
        Me.lblBonus.Text = "Bonus"
        Me.lblBonus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUpperTot
        '
        Me.lblUpperTot.AutoSize = True
        Me.lblUpperTot.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpperTot.Location = New System.Drawing.Point(162, 355)
        Me.lblUpperTot.Name = "lblUpperTot"
        Me.lblUpperTot.Size = New System.Drawing.Size(74, 13)
        Me.lblUpperTot.TabIndex = 99
        Me.lblUpperTot.Text = "Upper Total"
        Me.lblUpperTot.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRolls
        '
        Me.lblRolls.BackColor = System.Drawing.Color.Transparent
        Me.lblRolls.Location = New System.Drawing.Point(12, 645)
        Me.lblRolls.Name = "lblRolls"
        Me.lblRolls.Size = New System.Drawing.Size(90, 16)
        Me.lblRolls.TabIndex = 97
        Me.lblRolls.Text = "Rolls: x of 3"
        Me.lblRolls.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnRoll
        '
        Me.btnRoll.Location = New System.Drawing.Point(12, 614)
        Me.btnRoll.Name = "btnRoll"
        Me.btnRoll.Size = New System.Drawing.Size(90, 28)
        Me.btnRoll.TabIndex = 96
        Me.btnRoll.Text = "Roll"
        Me.btnRoll.UseVisualStyleBackColor = True
        '
        'pctLogo
        '
        Me.pctLogo.BackColor = System.Drawing.Color.Transparent
        Me.pctLogo.Image = CType(resources.GetObject("pctLogo.Image"), System.Drawing.Image)
        Me.pctLogo.Location = New System.Drawing.Point(12, 27)
        Me.pctLogo.Name = "pctLogo"
        Me.pctLogo.Size = New System.Drawing.Size(138, 94)
        Me.pctLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pctLogo.TabIndex = 95
        Me.pctLogo.TabStop = False
        '
        'frmHiResGame
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(394, 674)
        Me.Controls.Add(Me.lblChance)
        Me.Controls.Add(Me.lblYahtzee)
        Me.Controls.Add(Me.btnYahtzee)
        Me.Controls.Add(Me.lblLargeStraight)
        Me.Controls.Add(Me.lblSmallStraight)
        Me.Controls.Add(Me.lblFullHouse)
        Me.Controls.Add(Me.lbl4ofKind)
        Me.Controls.Add(Me.lblSixes)
        Me.Controls.Add(Me.lbl3ofKind)
        Me.Controls.Add(Me.lblFives)
        Me.Controls.Add(Me.lblFours)
        Me.Controls.Add(Me.lblThrees)
        Me.Controls.Add(Me.lblTwos)
        Me.Controls.Add(Me.lblAces)
        Me.Controls.Add(Me.lblYahtzeeBonus)
        Me.Controls.Add(Me.lblGrandTot)
        Me.Controls.Add(Me.lblLowerTot)
        Me.Controls.Add(Me.lblBonus)
        Me.Controls.Add(Me.lblUpperTot)
        Me.Controls.Add(Me.lblRolls)
        Me.Controls.Add(Me.btnRoll)
        Me.Controls.Add(Me.pctLogo)
        Me.Controls.Add(Me.MenuStrip1)
        Me.hlpYahtzee.SetHelpKeyword(Me, "120")
        Me.hlpYahtzee.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.TopicId)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmHiResGame"
        Me.hlpYahtzee.SetShowHelp(Me, True)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Yahtzee"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.pctLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tmrRollAni As System.Windows.Forms.Timer
    Friend WithEvents YahtzeeHelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblChance As System.Windows.Forms.Label
    Friend WithEvents lblYahtzee As System.Windows.Forms.Label
    Friend WithEvents btnYahtzee As System.Windows.Forms.Button
    Friend WithEvents lblLargeStraight As System.Windows.Forms.Label
    Friend WithEvents lblSmallStraight As System.Windows.Forms.Label
    Friend WithEvents lblFullHouse As System.Windows.Forms.Label
    Friend WithEvents lbl4ofKind As System.Windows.Forms.Label
    Friend WithEvents lblSixes As System.Windows.Forms.Label
    Friend WithEvents lbl3ofKind As System.Windows.Forms.Label
    Friend WithEvents lblFives As System.Windows.Forms.Label
    Friend WithEvents lblFours As System.Windows.Forms.Label
    Friend WithEvents lblThrees As System.Windows.Forms.Label
    Friend WithEvents lblTwos As System.Windows.Forms.Label
    Friend WithEvents lblAces As System.Windows.Forms.Label
    Friend WithEvents lblYahtzeeBonus As System.Windows.Forms.Label
    Friend WithEvents lblGrandTot As System.Windows.Forms.Label
    Friend WithEvents lblLowerTot As System.Windows.Forms.Label
    Friend WithEvents lblBonus As System.Windows.Forms.Label
    Friend WithEvents lblUpperTot As System.Windows.Forms.Label
    Friend WithEvents lblRolls As System.Windows.Forms.Label
    Friend WithEvents btnRoll As System.Windows.Forms.Button
    Friend WithEvents pctLogo As System.Windows.Forms.PictureBox
    Friend WithEvents YahtzeeRulesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents hlpYahtzee As System.Windows.Forms.HelpProvider
    Friend WithEvents HighScoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator

End Class
