﻿Public Class frmMainMenu

    Private Sub frmMainMenu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If My.Settings.exitConfirm = True Then
            'Confirm exit of game
            If MessageBox.Show("Are you sure you want to exit Yahtzee?", My.Application.Info.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmMainMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If My.Settings.firstRun = True Then
            'Set game screen size based on current screen resolution - hires or lowres
            My.Settings.gameSize = frmOptions.gameSize

            'Set firstRun to false, so no automatic game screen change from now on
            My.Settings.firstRun = False
        End If

        'Set help file location
        hlpYahtzee.HelpNamespace = My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm"
    End Sub

    Private Sub rbOnePlayer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOnePlayer.CheckedChanged
        'One (1) player game
        txtPlayer1.Visible = True
        txtPlayer2.Visible = False
        txtPlayer3.Visible = False
        txtPlayer4.Visible = False
    End Sub

    Private Sub rbTwoPlayers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbTwoPlayers.CheckedChanged
        'Two (2) player game
        txtPlayer1.Visible = True
        txtPlayer2.Visible = True
        txtPlayer3.Visible = False
        txtPlayer4.Visible = False
    End Sub

    Private Sub rbThreePlayers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbThreePlayers.CheckedChanged
        'Three (3) player game
        txtPlayer1.Visible = True
        txtPlayer2.Visible = True
        txtPlayer3.Visible = True
        txtPlayer4.Visible = False
    End Sub

    Private Sub rbFourPlayers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbFourPlayers.CheckedChanged
        'Four (4) player game
        txtPlayer1.Visible = True
        txtPlayer2.Visible = True
        txtPlayer3.Visible = True
        txtPlayer4.Visible = True
    End Sub

    Private Sub btnHighScores_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHighScores.Click
        'Show high scores
        frmHighScores.Show()
    End Sub

    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        'Show load game screen
        frmLoadGame.Show()
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        'Show help - Yahtzee help
        Help.ShowHelp(Me, My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm", HelpNavigator.TopicId, "100")
    End Sub

    Private Sub btnRules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRules.Click
        'Show help - Yahtzee rules
        Help.ShowHelp(Me, My.Computer.FileSystem.CurrentDirectory & "\GameData\Yahtzee.chm", HelpNavigator.TopicId, "200")
    End Sub

    Private Sub btnOptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOptions.Click
        'Show options
        frmOptions.Show()
    End Sub

    Private Sub btnAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAbout.Click
        'Show about box
        frmAboutBox.Show()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        Dim intPlayers As Integer
        Dim strNames(3) As String

        'Get number of players, and names of players
        If rbOnePlayer.Checked = True Then
            strNames(0) = txtPlayer1.Text.ToString
            intPlayers = 1
        ElseIf rbTwoPlayers.Checked = True Then
            strNames(0) = txtPlayer1.Text.ToString
            strNames(1) = txtPlayer2.Text.ToString
            intPlayers = 2
        ElseIf rbThreePlayers.Checked = True Then
            strNames(0) = txtPlayer1.Text.ToString
            strNames(1) = txtPlayer2.Text.ToString
            strNames(2) = txtPlayer3.Text.ToString
            intPlayers = 3
        ElseIf rbFourPlayers.Checked = True Then
            strNames(0) = txtPlayer1.Text.ToString
            strNames(1) = txtPlayer2.Text.ToString
            strNames(2) = txtPlayer3.Text.ToString
            strNames(3) = txtPlayer4.Text.ToString
            intPlayers = 4
        End If

        'Assign names to players if they don't have one
        For i As Integer = 0 To intPlayers - 1
            If strNames(i) = Nothing Then
                If MessageBox.Show("There is no name for Player " & i + 1 & "." & vbNewLine _
                    & "Would you like to use the default name: Player " & i + 1 & "?", _
                    My.Application.Info.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    strNames(i) = "Player " & i + 1
                Else
                    Exit Sub
                End If
            End If
        Next

        'Start a new game
        newGame(intPlayers, strNames)
    End Sub
End Class