﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHighScores
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHighScores))
        Me.lsvHighScores = New System.Windows.Forms.ListView
        Me.chPos = New System.Windows.Forms.ColumnHeader
        Me.chName = New System.Windows.Forms.ColumnHeader
        Me.chScore = New System.Windows.Forms.ColumnHeader
        Me.chYahtzees = New System.Windows.Forms.ColumnHeader
        Me.chDate = New System.Windows.Forms.ColumnHeader
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rbNoYaht = New System.Windows.Forms.RadioButton
        Me.rbWithYaht = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblNoScores = New System.Windows.Forms.Label
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.hlpYahtzee = New System.Windows.Forms.HelpProvider
        Me.GroupBox1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lsvHighScores
        '
        Me.lsvHighScores.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lsvHighScores.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.chPos, Me.chName, Me.chScore, Me.chYahtzees, Me.chDate})
        Me.lsvHighScores.FullRowSelect = True
        Me.lsvHighScores.Location = New System.Drawing.Point(13, 94)
        Me.lsvHighScores.Name = "lsvHighScores"
        Me.lsvHighScores.Size = New System.Drawing.Size(349, 209)
        Me.lsvHighScores.TabIndex = 0
        Me.lsvHighScores.UseCompatibleStateImageBehavior = False
        Me.lsvHighScores.View = System.Windows.Forms.View.Details
        '
        'chPos
        '
        Me.chPos.Text = "Pos"
        Me.chPos.Width = 30
        '
        'chName
        '
        Me.chName.Text = "Name"
        Me.chName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.chName.Width = 110
        '
        'chScore
        '
        Me.chScore.Text = "Score"
        Me.chScore.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.chScore.Width = 55
        '
        'chYahtzees
        '
        Me.chYahtzees.Text = "Yahtzees"
        Me.chYahtzees.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chDate
        '
        Me.chDate.Text = "Date"
        Me.chDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.chDate.Width = 80
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.rbNoYaht)
        Me.GroupBox1.Controls.Add(Me.rbWithYaht)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(349, 47)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "High Score Type"
        '
        'rbNoYaht
        '
        Me.rbNoYaht.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rbNoYaht.AutoSize = True
        Me.rbNoYaht.Location = New System.Drawing.Point(199, 19)
        Me.rbNoYaht.Name = "rbNoYaht"
        Me.rbNoYaht.Size = New System.Drawing.Size(109, 17)
        Me.rbNoYaht.TabIndex = 1
        Me.rbNoYaht.Text = "Without Yahtzees"
        Me.rbNoYaht.UseVisualStyleBackColor = True
        '
        'rbWithYaht
        '
        Me.rbWithYaht.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.rbWithYaht.AutoSize = True
        Me.rbWithYaht.Checked = True
        Me.rbWithYaht.Location = New System.Drawing.Point(45, 19)
        Me.rbWithYaht.Name = "rbWithYaht"
        Me.rbWithYaht.Size = New System.Drawing.Size(94, 17)
        Me.rbWithYaht.TabIndex = 0
        Me.rbWithYaht.TabStop = True
        Me.rbWithYaht.Text = "With Yahtzees"
        Me.rbWithYaht.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(115, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(145, 29)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "High Scores"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNoScores
        '
        Me.lblNoScores.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNoScores.BackColor = System.Drawing.SystemColors.Control
        Me.lblNoScores.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoScores.Location = New System.Drawing.Point(79, 131)
        Me.lblNoScores.Name = "lblNoScores"
        Me.lblNoScores.Size = New System.Drawing.Size(217, 13)
        Me.lblNoScores.TabIndex = 3
        Me.lblNoScores.Text = "There are currently no High Scores available"
        Me.lblNoScores.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblNoScores.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(6, 0)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(82, 23)
        Me.btnReset.TabIndex = 4
        Me.btnReset.Text = "Reset Scores"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(3, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(98, 309)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnReset)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnClose)
        Me.SplitContainer1.Size = New System.Drawing.Size(179, 24)
        Me.SplitContainer1.SplitterDistance = 91
        Me.SplitContainer1.TabIndex = 7
        '
        'frmHighScores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(374, 344)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.lblNoScores)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lsvHighScores)
        Me.HelpButton = True
        Me.hlpYahtzee.SetHelpKeyword(Me, "150")
        Me.hlpYahtzee.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.TopicId)
        Me.hlpYahtzee.SetHelpString(Me, "")
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(500, 500)
        Me.MinimumSize = New System.Drawing.Size(390, 382)
        Me.Name = "frmHighScores"
        Me.hlpYahtzee.SetShowHelp(Me, True)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Yahtzee - High Scores"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lsvHighScores As System.Windows.Forms.ListView
    Friend WithEvents chName As System.Windows.Forms.ColumnHeader
    Friend WithEvents chScore As System.Windows.Forms.ColumnHeader
    Friend WithEvents chYahtzees As System.Windows.Forms.ColumnHeader
    Friend WithEvents chDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents chPos As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbNoYaht As System.Windows.Forms.RadioButton
    Friend WithEvents rbWithYaht As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblNoScores As System.Windows.Forms.Label
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents hlpYahtzee As System.Windows.Forms.HelpProvider
End Class
